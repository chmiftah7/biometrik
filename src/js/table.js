//Initialize Select2 Elements
$('.select2').select2();


$(".company").select2({
    placeholder: 'Pilih perusahaan',
    allowClear: true,
    ajax: {
        headers: {
            'Authorization': 'Bearer ' + token
        },
        url: base_url + 'data-master/company',
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                type: 2,
                term: params.term || '',
                page: params.page || 1
            }
        },
        cache: true
    }
});

$(".department").select2({
    placeholder: 'Pilih Departemen',
    allowClear: true,
    ajax: {
        headers: {
            'Authorization': 'Bearer ' + token
        },
        url: base_url + 'data-master/department',
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                type: 2,
                term: params.term || '',
                page: params.page || 1
            }
        },
        cache: true
    }
});

$(".location").select2({
    placeholder: 'Pilih Lokasi',
    allowClear: true,
    ajax: {
        headers: {
            'Authorization': 'Bearer ' + token
        },
        url: base_url + 'data-master/location',
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                type: 2,
                term: params.term || '',
                page: params.page || 1
            }
        },
        cache: true
    }
});

//Data picker
$('.datepicker').datepicker({
    autoclose: true,
});

/* ========================================================== */
const startToday = moment().format('D MMM YYYY');
const endToday = moment().format('D MMM YYYY');
$('.daterange').val(startToday + '-' + endToday);
// console.log(startToday);
/* ========================================================== */

$('.daterange').daterangepicker({
    autoUpdateInput: false,
    locale: {
        "format": "DD MMM YYYY",
        "separator": "-",
        "cancelLabel": 'Clear'
    }
}, function (start, end, label) {
    $(".daterange").val(start.format('DD MMM YYYY') + '-' + end.format('DD MMM YYYY'));
});

$(".daterange").on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');
});


//get data tabel 

var page = 1;
var current_page = 3;
var total_page = 100;
var is_ajax_fire = 0;
var query = "?";


manageData(query);
$('.btn-filter').on('click', function (e) {

    var rows = ""
    rows = rows + '<tr>';
    rows = rows + '<td colspan="10" class="text-center"> <span class="text-center">sedang memuat data</span>';

    rows = rows + '</td>';
    rows = rows + '</tr>';
    $("#data-participant").html(rows);
    e.preventDefault()
    manageData(query);
})




/* tampilkan data */
function manageData(query) {
    // console.log('get data');
    $(`.loading`).removeClass('d-none');


    let status_foto = $('#status_foto').val();
    if (status_foto != '') {
        let q_status_foto = "status_foto=" + status_foto + '&';
        query += q_status_foto
    }

    let status_finger = $('#status_finger').val();
    if (status_finger != '') {
        let q_status_finger = "status_finger=" + status_finger + '&';
        query += q_status_finger
    }

    let status = $('#status').val();
    if (status != '') {
        let q_status = "status=" + status + '&';
        query += q_status
    }

    let location = $('#location').val();
    if (location != '') {
        let q_location = "location=" + location + '&';
        query += q_location
    }

    let department = $('#department').val();
    if (department != '') {
        let q_department = "department=" + department + '&';
        query += q_department
    }


    let company = $('#company').val();
    if (company != '') {
        let q_company = "company=" + company + '&';
        query += q_company
    }

    let search = $('#search').val();
    if (search != '') {
        let q_search = "search=" + search + '&';
        query += q_search
    }

    let date = $('#date').val();
    if (date != '') {
        let q_date = "date=" + date + '&';
        query += q_date
    }

    localStorage.setItem("query", query);
    let _query = localStorage.getItem("query");

    if (localStorage.getItem("query") != null) {
        query = _query
    }



    var $pagination = $('#pagination');
    var defaultOpts = {
        totalPages: 20
    };
    $pagination.twbsPagination(defaultOpts);
    $pagination.twbsPagination('destroy');
    $.ajax({
        headers: {
            'Authorization': 'Bearer ' + token
        },
        dataType: 'json',
        url: base_url + 'participant' + query,
        data: {
            page: page
        }
    }).done(function (data) {

        if (data.data.length == 0) {

            $(`.loading`).addClass('d-none');
            $('.page-from').html(0)
            $('.page-to').html(0)
            $('.page-total').html(0)
            $('.loader-text').html('Tidak Ada Data')

            var rows = ""
            rows = rows + '<tr>';
            rows = rows + '<td colspan="10" class="text-center"> <span class="text-center">tidak ada data</span>';

            rows = rows + '</td>';
            rows = rows + '</tr>';
            $("#data-participant").html(rows);
        } else {
            $('.page-from').html(data.meta.from)
            $('.page-to').html(data.meta.to)
            $('.page-total').html(data.meta.total)



            total_page = Math.ceil(data.meta.total / 10);
            current_page = page;


            manageRow(data.data);
            is_ajax_fire = 1;
            $(`.loading`).addClass('d-none');


            var defaultPage = localStorage.getItem('currentPage') ?
                parseInt(localStorage.getItem('currentPage')) : 1;
            $('#pagination').twbsPagination({
                totalPages: total_page,
                first: '',
                last: '',
                prev: '<span aria-hidden="true"><i class="fa fa-angle-left"></i></span>',
                next: '<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>',
                visiblePages: 5,
                startPage: defaultPage,
                onPageClick: function (event, pageL) {
                    page = pageL;
                    // console.log('current_page', pageL);
                    localStorage.setItem('currentPage', page);

                    if (is_ajax_fire != 0) {
                        getPageData(query);
                    }
                }
            });
        }



    });
}

/* tampilkan  data */
function getPageData(query) {
    // localStorage.setItem('firstLoad', true);
    // let isFirstReload = JSON.parse(localStorage.getItem('firstLoad'));

    // console.log(isFirstReload);

    // isFirstReload === true ? 
    //     localStorage.setItem('firstLoad', false) :
    $(`.loading`).removeClass('d-none');

    // console.log(isFirstReload);

    // isFirstReload === false ? 
    //     $(`.loading`).removeClass('d-none') : ''; 

    let status_foto = $('#status_foto').val();
    if (status_foto != '') {
        let q_status_foto = "status_foto=" + status_foto + '&';
        query += q_status_foto
    }

    let status_finger = $('#status_finger').val();
    if (status_finger != '') {
        let q_status_finger = "status_finger=" + status_finger + '&';
        query += q_status_finger
    }


    let status = $('#status').val();
    if (status != '') {
        let q_status = "status=" + status + '&';
        query += q_status
    }

    let location = $('#location').val();
    if (location != '') {
        let q_location = "location=" + location + '&';
        query += q_location
    }

    let department = $('#department').val();
    if (department != '') {
        let q_department = "department=" + department + '&';
        query += q_department
    }

    let company = $('#company').val();
    if (company != '') {
        let q_company = "company=" + company + '&';
        query += q_company
    }

    let search = $('#search').val();
    if (search != '') {
        let q_search = "search=" + search + '&';
        query += q_search
    }

    let date = $('#date').val();
    if (date != '') {
        let q_date = "date=" + date + '&';
        query += q_date
    }


    localStorage.setItem("query", query);
    let _query = localStorage.getItem("query");


    if (localStorage.getItem("query") != null) {
        query = _query
    }

    $.ajax({
        headers: {
            'Authorization': 'Bearer ' + token
        },
        dataType: 'json',
        url: base_url + 'participant' + query,
        data: {
            page: page,
            query: query
        }
    }).done(function (data) {
        // isFirstReload === true ? 
        //     localStorage.setItem('firstLoad', false) :
        $(`.loading`).addClass('d-none');
        $('.page-from').html(data.meta.from)
        $('.page-to').html(data.meta.to)
        $('.page-total').html(data.meta.total)
        manageRow(data.data);
    });
}

function manageRow(data) {
    var rows = "";

    $.each(data, function (key, value) {
        rows += `
            <tr>
                <td>
                    <span class="action action-left" style="display: flex">
                        ${!value.is_take_finger ? `
                            <a href="#" title="Input Sidik Jari" id="createEnrollmentButton1" class="fingerprint-input"   data-toggle="modal" data-target="#createEnrollment1" onclick="beginEnrollment()" data-value="${value.id}">
                            <i class="mdi mdi-fingerprint"></i>
                        </a>`: ``}

                        <a href="#" title="Verifikasi/Take Foto " class="fingerprint-take-photo"  data-toggle="st-popup" data-target="#take-photo" data-value="${value.id}">
                            <i class="fa fa-camera"></i>
                            </a>
                        ${value.is_take_finger ? `
                        <a href="#" title="Verifikasi Data Sidik Jari By User"  id="verifyIdentityButton" class="fingerprint-verification" data-toggle="modal" data-target="#verifyIdentity" onclick="beginIdentification()" data-value="${value.id}">
                            <i class="fa fa-check-circle-o"></i>
                        </a>
                        ` : "" }
                        ${value.is_take_finger ? `
                        <a href="#" class="hapus-data-biometric" title="Clear Data Biometric"  data-value="${value.user_id}" >
                            <i class="fa fa-times-circle-o"></i>
                        </a>`: ""}
                    </span>
                </td>
                <td> <a href="#" title="detail peserta" class="detail-participant" data-toggle="st-popup" data-target="#detail" data-value="${value.id}">${value.name}</a></td>
                <td>${value.nik}</td>;
                <td>${value.company}</td>;
                <td>${value.department}</td>;
                <td>${value.implementation_date}</td>;
                <td>${value.location}</td>;
                <td>  ${value.is_take_photo ?
                "<span class='label label-finish'>lengkap</span>" : "<span class='label label-high'>belum rekam/ upload foto</span>"} 
                </td>
                <td>  ${value.is_take_finger ?
                "<span class='label label-finish'>lengkap</span>" : "<span class='label label-high'>belum rekam sidik jari</span>"} 
                </td>
                <td>  ${value.is_verification ?
                "<span class='label label-finish'>hadir</span>" : "<span class='label label-high'>belum hadir</span>"} 
                </td>
            </tr>
        `;
    });
    $("#data-participant").html(rows);
    hapusDataBiometric();



    $('.detail-participant').on('click', function () {
        resetTable()
        $(`.loading`).removeClass('d-none');
        let participantId = $(this).data('value');

        $.ajax({
            headers: {
                'Authorization': 'Bearer ' + token
            },
            url: base_url + 'participant/' + participantId,
            type: "get",
            dataType: "JSON",
            success: function (response) {
                $(`.loading`).addClass('d-none');
                $('#userID').val(response.data.user_id)
                appendData(response)
            },
            error: function (response) { }

        });

    })

    $('.fingerprint-input').on('click', function () {
        resetTable()
        $(`.loading`).removeClass('d-none');
        let participantId = $(this).data('value');

        $.ajax({
            headers: {
                'Authorization': 'Bearer ' + token
            },
            url: base_url + 'participant/' + participantId,
            type: "get",
            dataType: "JSON",
            success: function (response) {
                $(`.loading`).addClass('d-none');
                $('#userID').val(response.data.user_id)
                appendData(response)
            },
            error: function (response) { }

        });

    })

    $('.fingerprint-take-photo').on('click', function () {
        resetTable()
        $(`.loading`).removeClass('d-none');
        let participantId = $(this).data('value');
        $.ajax({
            headers: {
                'Authorization': 'Bearer ' + token
            },
            url: base_url + 'participant/' + participantId,
            type: "get",
            dataType: "JSON",
            success: function (response) {
                $(`.loading`).addClass('d-none');
                document.getElementById("foto_permit").src = response.data.photo_url;
                $('.participant-id').val(response.data.id)
                $('.participant-user-id').val(response.data.user_id)
                appendData(response)
            },
            error: function (response) { }

        });
    })

    $('.fingerprint-verification').on('click', function () {
        resetTable()
        $(`.loading`).removeClass('d-none');
        let participantId = $(this).data('value');
        $('#userID').val('')

        $.ajax({
            headers: {
                'Authorization': 'Bearer ' + token
            },
            url: base_url + 'participant/' + participantId,
            type: "get",
            dataType: "JSON",
            success: function (response) {
                $(`.loading`).addClass('d-none');
                $('#userIDVerify').val(response.data.user_id)
                $('#participantId').val(response.data.id)
                appendData(response)
            },
            error: function (response) { }

        });
    })

}

function hapusDataBiometric() {
    $('.hapus-data-biometric').on('click', function () {

        let userId = $(this).data('value');
        let title = "Apakah Anda Yakin Ingin Menghapus Data Rekam Biometric ?";
        let icon = "warning";
        let iconColor = "#ebbd21";
        let confirmButtonText = "<i class='fa fa-check-circle-o'></i> Ya, Hapus";
        let confirmButtonColor = "#399E6B";
        let showCancelButton = true;
        let cancelButtonText = "BATAL";
        let cancelButtonColor = "#eee";

        Swal.fire({
            title,
            icon,
            iconColor,
            confirmButtonText,
            confirmButtonColor,
            showCancelButton,
            cancelButtonText,
            cancelButtonColor,

        }).then((result) => {
            let pesanBerhasil = "Data Rekam Biometric Berhasil Dihapus!";
            let kategori = "success";
            let pesanGagal = "Data Rekam Biometric Gagal Dihapus!";
            let kategoriGagal = "error";

            if (result.isConfirmed) {
                $.ajax({
                    headers: {
                        'Authorization': 'Bearer ' + token
                    },
                    url: base_url + 'participant/' + userId + '/delete',
                    type: "get",
                    dataType: "JSON",
                    success: function (response) {
                        Swal.fire(pesanBerhasil, "", kategori);
                        // location.reload();
                        manageData(query);
                        localStorage.setItem('updateData', true);
                        localStorage.getItem('updateData') ?
                            $('.daterange').val('') : null;


                        var defaultPage = localStorage.getItem('currentPage') ?
                            parseInt(localStorage.getItem('currentPage')) : 1;
                        $('#pagination').twbsPagination({
                            totalPages: total_page,
                            first: '<span aria-hidden="true">&laquo;</span>',
                            last: '<span aria-hidden="true">&raquo;</span>',
                            prev: '<span aria-hidden="true">&lt;</span>',
                            next: '<span aria-hidden="true">&gt;</span>',
                            visiblePages: 3,
                            startPage: defaultPage,
                            onPageClick: function (event, pageL) {
                                page = pageL;
                                // console.log('current_page', pageL);
                                localStorage.setItem('currentPage', page);

                                if (is_ajax_fire != 0) {
                                    getPageData(query);
                                }
                            }
                        });

                    },
                    error: function (response) {
                        console.log(response);
                        Swal.fire(pesanGagal, "", kategoriGagal);
                        // location.reload();
                    }
                });
            }

        });
    })
}

function appendData(response) {

    $('.data-name').html(response.data.name)
    $('.data-nik').html(response.data.nik)
    $('.data-permit_number').html(response.data.minepermit_number)
    $('.data-company').html(response.data.company)
    $('.data-position').html(response.data.position)
    $('.data-department').html(response.data.department)
    $('.data-implementation-date').html(response.data.implementation_date)
    $('.data-implementation-date-hours').html(response.data.implementation_date_hours)
    $('.data-location').html(response.data.location)
    if (response.data.is_take_finger == true) {
        $('.data-status-finger').html('<span class="label label-finish">Sudah</span>')
    } else {
        $('.data-status-finger').html('<span class="label label-high">Belum</span>')
    }
    if (response.data.is_verification == true) {
        $('.data-status-verification').html('<span class="label label-finish">Sudah</span>')
    } else {
        $('.data-status-verification').html('<span class="label label-high">Belum</span>')
    }
    if (response.data.is_take_photo == true) {
        $('.data-status-photo').html('<span class="label label-finish">Sudah</span>')
    } else {
        $('.data-status-photo').html('<span class="label label-high">Belum</span>')

    }
}

function resetTable() {

    $('#foto_permit').attr("src", '');
    $('.data-name').html('')
    $('.data-nik').html('')
    $('.data-permit_number').html('')
    $('.data-company').html('')
    $('.data-position').html('')
    $('.data-department').html('')
    $('.data-implementation-date').html('')
    $('.data-implementation-date-hours').html('')
    $('.data-location').html('')
    $('.data-status-finger').html('')
    $('.data-status-verification').html('')
    $('.data-status-photo').html('')
}
