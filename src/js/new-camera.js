$(document).ready(function () {
    // $(".st-popup-wrap").on("show.st-popup", function(e) {
    //     const id = e.target.getAttribute('id');
    //     console.log(id);
    // });

    //handle upload image
    imgInp.onchange = async (evt) => {
        const [file] = imgInp.files
        if (file) {
            event.preventDefault();
            canvas.width = 400;
            canvas.height = 500;
            context.scale(-1, 1);
            context.drawImage(video, 130, 5, 400, 470, -400, 0, 420, 500);
            result = await convertBase64(file);
            const resized = await reduce_image_file_size(result)
            resultb64 = resized
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            $("#captured_image").prop('src', resultb64).show();
            $("#captured_image").width('auto');
            $("#captured_image").height(300);
            $('#vid').css('z-index', '20').hide();
            $('#capture').css('z-index', '30').hide();

            console.log(123); // ok
            $('#snap').prop('disabled', true);
            $('#frontBackCamera').prop('disabled', true);
            // $("#uploadPhoto")
            $('#uploadPhoto').css({'cursor': 'not-allowed', 'background-color': '#88c5a6'});
            $('#imgInp').prop('disabled', true);
            // $("#uploadPhoto")
            // $('#verifikasi').prop('disabled', false);
            // $("#retake").prop('disabled', false);
            closeCam();
        }
    }

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            fileReader.onload = () => {
                resolve(fileReader.result);
            };

            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };

    //js camera
    $('#video').resize(function () {
        $('#cont').height($('#video').height());
        $('#cont').width($('#video').width());
        $('#control').height($('#video').height() * 0.1);
        $('#control').css('top', $('#video').height() * 0.9);
        $('#control').width($('#video').width());
        $('#control').show();
    });

    // make default photos
    var dataImage = null; //localStorage.getItem('images');
    var img = $('<img" width="100%" height="100%" id="img_menu-item" style="height: 100%;width:100%">');
    jQuery('#pg_home_content').append(img)
    if (dataImage) {
        bannerImg = document.getElementById('foto_permit');
        bannerImg.src = dataImage;
        jQuery('#pg_home_content').append(bannerImg);
    } else {
        bannerImage = document.getElementById("img_menu-item");
        img[0].src = "{{ $minePermit->photo_url ?? asset('assets/img/user/default.png') }}";
    };
    let mode = "environment";
    $('#frontBackCamera').click(function () {
        $(this).text(
            $(this).text() == 'KAMERA BELAKANG' ?
                'KAMERA DEPAN' : 'KAMERA BELAKANG'
        );
        mode =
            $(this).text() == 'KAMERA BELAKANG' ?
                'user' : 'environment';
        closeCam();
        openCam(mode);
        $('.camera-control').show();
        $("#camera").show();
        $("#profile-picture").hide();
        $('#vid').css('z-index', '30').show();
        $('#capture').css('z-index', '20').hide();
        $("#captured_image").hide();
    })

    // check camera avaliable from broweser
    function openCam(mode) {
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator
            .mozGetUserMedia || navigator.oGetUserMedia || navigator.msGetUserMedia;
        if (navigator.getUserMedia) {
            navigator.getUserMedia({
                // video: true,
                // facingMode: 'user',
                video: {
                    facingMode: mode
                }

            }, streamWebCam, throwError);
        }
    }

    $('.popup-close').click(function (event) {
        event.preventDefault();
        closeCam()
    })

    function closeCam() {
        video.pause();
        try { video.srcObject = null; } 
        catch (error) { video.src = null; }
        let track = strr?.getTracks()[0];
        track?.stop();
    }

    // make camera from video tag
    const video = document.getElementById('video');
    const canvas = document.getElementById('canvas');
    const context = canvas.getContext('2d');
    let strr;

    function streamWebCam(stream) {
        const mediaSource = new MediaSource(stream);
        try {
            video.srcObject = stream;
        } catch (error) {
            video.src = URL.createObjectURL(mediaSource);
        }
        video.play();
        strr = stream;

        if (screen.width < 700) {
            setTimeout(() => {
                const frameHeight = $('.frame-photos').height();
                $(".frame").css({
                    "border": 'none',
                    "width": video.clientWidth + 'px',
                    "height": `${frameHeight + 50}px`,
                    "bottom": `${frameHeight + 50}px`,
                });
            }, 500);
        }
    }

    function throwError(e) {
        alert(e.name);
    }

    $('#open').click(function (event) {
        let participantId = $('.participant-id').val();
        let participantUuid = $('.participant-uuid').val();
        $('#save').removeClass('d-none');
        event.preventDefault();
        openCam(mode);
        $('.camera-control').show();
        $("#camera").show();
        $("#profile-picture").hide();
        $('#vid').css('z-index', '30').show();
        $('#capture').css('z-index', '20').hide();
        $("#captured_image").hide();
        $("#save").show();
    });

    $('#close').click(function (event) {
        event.preventDefault();
        closeCam();
    });

    $('#snap').click(function (event) {
        $("#captured_image").attr('height', '');
        $("#captured_image").attr('width', '');
        event.preventDefault();
        context.scale(-1, 1);

        // check if user using mobile device or pc
        canvas.width = video.clientWidth;
        canvas.height = video.clientHeight;
        if (isMobileDevice()) {
            // alert("User is on a mobile device");
            // context.drawImage(video, 
            //     10, 100, 800, 500,
            //     -400, 0, 800, 500
            // ); // mobile
            context.drawImage(video, 0, 0, video.clientWidth, video.clientHeight);
        } else {
            context.drawImage(video, 0, 0, video.clientWidth, video.clientHeight);
            $("#captured_image").width(video.clientWidth).height(video.clientHeight)
            // alert("User is on a PC");
            // canvas.width = 400;
            // canvas.height = 500;
        //     context.drawImage(video, 
        //         130, 0, 400, 480, 
        //         0, 0, 410, 500
        //     ); // pc
        }
        //context.drawImage(video, 10,10);
        resultb64 = canvas.toDataURL("image/jpeg");
        // console.log(resultb64);
        $("#captured_image").prop('src', resultb64).show();
        $('#vid').css('z-index', '20').hide();
        $('#capture').css('z-index', '30').hide();

        // function to check if user using mobile device or pc
        function isMobileDevice() {
            return /Mobi|Android|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
        }
    });

    $('#verifikasi').click(function (e) {
        $(`.loading`).removeClass('d-none');

        // localStorage.setItem("images", resultb64);
        if (noImagePhoto() == false) return;

        let img = document.getElementById('foto_permit');
        img.src = resultb64;

        setTimeout(() => {
            verifyPhoto(resultb64, e);
        }, 500);
    })

    // verify photo
    function verifyPhoto(file, e) {
        let participantId = $('.participant-id').val();
        let formData = new FormData();

        formData.append('file', file);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Authorization':'Bearer ' + token
            },
            async: false,
            url: base_url + 'participant/' + participantId + '/verify-photo',
            data: formData,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {

                // $(`.loading`).addClass('d-none');
                console.log(data);
                if (data.status == 'verified') {

                    let title = "Verifikasi Foto Berhasil. Akurasi : " + data.accuracy + '%';
                    let icon = "success";
                    let iconColor = "#399E6B";
                    let confirmButtonText = "<i class='fa fa-check-circle-o'></i> OK";
                    let confirmButtonColor = "#399E6B";

                    Swal.fire({
                        title,
                        icon,
                        iconColor,
                        confirmButtonText,
                        confirmButtonColor
                    });


                    // UPLOAD FOTO
                    localStorage.setItem("images", resultb64);
                    let img = document.getElementById('foto_permit');
                    img.src = resultb64;

                    let participantId = $('.participant-id').val();
                    let app = 'adainduksi-v2/induction/participant/mine-permit';
                    let fileData = resultb64;
                    console.log(fileData);
                    let location =
                        `uploads/adainduksi-v2/induksi/partisipan/${participantId}/mine-permit/foto/`;
                    let fileName = `foto-${participantId}-${Date.now()}`;


                    function dataURLtoFile(dataurl, filename) {

                        var arr = dataurl.split(','),
                            mime = arr[0].match(/:(.*?);/)[1],
                            bstr = atob(arr[1]),
                            n = bstr.length,
                            u8arr = new Uint8Array(n);

                        while (n--) {
                            u8arr[n] = bstr.charCodeAt(n);
                        }

                        return new File([u8arr], filename, {
                            type: mime
                        });
                    }

                    var file = dataURLtoFile(fileData, 'tes.jpg')

                    let formData = new FormData();
                    formData.append('app', app);
                    formData.append('file', file);
                    formData.append('fileName', fileName);
                    formData.append('location', location);


                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            'Authorization':'Bearer ' + token
                        },
                        async: false,
                        url: base_url + 'participant/' + participantId + '/take-photo',
                        data: formData,
                        type: 'POST',
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {

                        },
                        error: function (err) {


                        }
                    });

                    //END UPLOAD
                    window.sandstrapPopup.close();

                    var page = 1;
                    var is_ajax_fire = 0;
                    var query = "?";


                    manageData(query)
                    var defaultPage = localStorage.getItem('currentPage') ? 
                        parseInt(localStorage.getItem('currentPage')) : 1;
                    $('#pagination').twbsPagination({
                        totalPages: total_page,
                        first: '<span aria-hidden="true">&laquo;</span>',
                        last: '<span aria-hidden="true">&raquo;</span>',
                        prev: '<span aria-hidden="true">&lt;</span>',
                        next: '<span aria-hidden="true">&gt;</span>',
                        visiblePages: 3,
                        startPage: defaultPage,
                        onPageClick: function (event, pageL) {
                            page = pageL;
                            // console.log('current_page', pageL);
                            localStorage.setItem('currentPage', page);
                            
                            if (is_ajax_fire != 0) {
                                getPageData(query);
                            }
                        }
                    });


                    setTimeout(function () {
                        //  manageData(query)

                        // window.location.reload();
                    }, 3000)

                    $(`.loading`).addClass('d-none');
                } else {
                    let title = `Verifikasi Foto Gagal Akurasi: ${!data.accuracy ? '0' : data.accuracy}%`;
                    let icon = "error";
                    let iconColor = "#ff4d4d";
                    let confirmButtonText = "<i class='fa fa-times-circle-o'></i> OK";
                    let confirmButtonColor = "#399E6B";

                    Swal.fire({
                        title,
                        icon,
                        iconColor,
                        confirmButtonText,
                        confirmButtonColor
                    });

                    $(`.loading`).addClass('d-none');
                }

                // setTimeout(function () {
                //     window.location.reload();
                // }, 5000)
            },
            error: function (err) {

                $(`.loading`).addClass('d-none');
                let title = "Terjadi Kesalahan Saat Verifikasi Foto";
                let icon = "error";
                let iconColor = "#ff4d4d";
                let confirmButtonText = "<i class='fa fa-times-circle-o'></i> OK";
                let confirmButtonColor = "#399E6B";

                Swal.fire({
                    title,
                    icon,
                    iconColor,
                    confirmButtonText,
                    confirmButtonColor
                });

                // setTimeout(function () {
                //     window.location.reload();
                // }, 3000);
            }
        });
    }

    // save photos from canvas then save to local storage
    $('#save').click(function (event) {
        event.preventDefault();
        $(`.loading`).removeClass('d-none');
        
        if (noImagePhoto() == false) return;
        localStorage.setItem("images", resultb64);
        let img = document.getElementById('foto_permit');
        img.src = resultb64;

        $('#vid').css('z-index', '30');
        $('#capture').css('z-index', '20');
        $("#camera").hide();
        $("#profile-picture").show();
        uploadPhoto(resultb64, event);
    });

    $('#save-modal').click(function (event) {
        resultb64 = canvas.toDataURL("image/jpeg");
        localStorage.setItem("images", resultb64);
        let img = document.getElementById('foto_permit');
        img.src = resultb64;
        uploadPhoto(resultb64);
        // window.sandstrapPopup.close()
    });

    $('#retake').click(function (event) {
        event.preventDefault();
        $('#vid').css('z-index', '30').show();
        $('#capture').css('z-index', '20').hide();
        $("#captured_image").hide();
        $('#snap').show();
        $("#snap").prop('disable', false);
        $("#retake").prop('disable', true);

        if (noImagePhoto() == false) {
            $('#imgInp').attr('src', '');
            $('#captured_image').attr('src', '');
            return;
        }

        openCam('environment');
        $('#imgInp').attr('src', '');

        $('#captured_image').attr('src', '');
    });

    $("#uploadPhoto").on("click", function() {
        // $('#imgInp').attr('src', '');
        $('#imgInp').val(null);
    });

    // Upload Foto
    function uploadPhoto(file) {

        console.log(123);
        let participantId = $('.participant-id').val();
        let app = 'adainduksi-v2/induction/participant/mine-permit';
        let fileData = file;
        let location =
            `uploads/adainduksi-v2/induksi/partisipan/${participantId}/mine-permit/foto/`;
        let fileName = `foto-${participantId}-${Date.now()}`;


        function dataURLtoFile(dataurl, filename) {

            var arr = dataurl.split(','),
                mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]),
                n = bstr.length,
                u8arr = new Uint8Array(n);

            while (n--) {
                u8arr[n] = bstr.charCodeAt(n);
            }

            return new File([u8arr], filename, {
                type: mime
            });
        }


        var file = dataURLtoFile(fileData, 'tes.jpg');


        let formData = new FormData();
        formData.append('app', app);
        formData.append('file', file);
        formData.append('fileName', fileName);
        formData.append('location', location);



        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Authorization':'Bearer ' + token
            },
            async: false,
            url: base_url + 'participant/' + participantId + '/take-photo',
            data: formData,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                $(`.loading`).addClass('d-none');
                let title = "Proses Simpan Foto Berhasil";
                let icon = "success";
                let iconColor = "#399E6B";
                let confirmButtonText = "<i class='fa fa-check-circle-o'></i> OK";
                let confirmButtonColor = "#399E6B";

                Swal.fire({
                    title,
                    icon,
                    iconColor,
                    confirmButtonText,
                    confirmButtonColor
                });

                

                var query = "?";
                manageData(query);
                localStorage.setItem('updateData', true);
                localStorage.getItem('updateData') ? 
                    $('.daterange').val('') : null;
                
                setTimeout(function () {
                    // window.location.reload();
                    Swal.close();
                    window.sandstrapPopup.close();
                }, 3000);
            },
            error: function (err) {

                $(`.loading`).addClass('d-none');
                let title = "Terjadi Kesalahan Saat Simpan Foto";
                let icon = "error";
                let iconColor = "#ff4d4d";
                let confirmButtonText = "<i class='fa fa-times-circle-o'></i> OK";
                let confirmButtonColor = "#399E6B";

                Swal.fire({
                    title,
                    icon,
                    iconColor,
                    confirmButtonText,
                    confirmButtonColor
                });

                setTimeout(function () {
                    // window.location.reload();
                    Swal.close();
                }, 3000);
            }
        });
    }





    /* file upload */
    fileInput = $(document).find('.form-file__input');
    fileInput.each(function () {
        var uploadInput = $(this).find('.input-upload');
        var uploadLabel = $(this).find('.form-file__label');

        uploadInput.change(function () {
            var fileName = uploadInput.val().replace(/.*[\/\\]/, '');
            uploadLabel.text(fileName);
        });
    })


    function fixWebCamBug() {

        $("#open").click(function () {
            $('#snap').prop('disabled', false);
            $('#frontBackCamera').prop('disabled', false);
            // $("#uploadPhoto")
            $('#uploadPhoto').css({'cursor': 'auto', 'background-color': '#399e6b'});
            $('#imgInp').prop('disabled', false);
            $('#captured_image').prop('src', '');
            $("#save").show();
            // $("#uploadPhoto")
            // $('#verifikasi').prop('disabled', true);
            // $("#retake").prop('disabled', true);
        })

        $("#snap").click(function () {
            $('#snap').prop('disabled', true);
            $('#frontBackCamera').prop('disabled', true);
            // $("#uploadPhoto")
            $('#uploadPhoto').css({'cursor': 'not-allowed', 'background-color': '#88c5a6'});
            $('#imgInp').prop('disabled', true);
            // $("#uploadPhoto")
            // $('#verifikasi').prop('disabled', false);
            // $("#retake").prop('disabled', false);
        });

        $("#retake").click(function () {
            $('#snap').prop('disabled', false);
            $('#frontBackCamera').prop('disabled', false);
            // $("#uploadPhoto")
            $('#uploadPhoto').css({'cursor': 'auto', 'background-color': '#399e6b'});
            $('#imgInp').prop('disabled', false);
            $('#captured_image').prop('src', '');
            // $("#uploadPhoto")
            // $('#verifikasi').prop('disabled', true);
            // $("#retake").prop('disabled', true);
        });


        $(document).mouseup(function(e) {
            $popupHeader = $(".popup-header");
            $popupBody = $(".popup-body");

            if (
                !$popupHeader.is(e.target) && 
                $popupHeader.has(e.target).length === 0 && 
                !$popupBody.is(e.target) && 
                $popupBody.has(e.target).length === 0
            ) {
                $("#camera").hide();
                $("#profile-picture").show();
                // console.log('click outside');
                closeCam();
                return;
            }
            
        });
    };
    fixWebCamBug();
});


/**
 * Resize a base 64 Image
 * @param {String} base64Str - The base64 string (must include MIME type)
 * @param {Number} MAX_WIDTH - The width of the image in pixels
 * @param {Number} MAX_HEIGHT - The height of the image in pixels
 */
async function reduce_image_file_size(base64Str, MAX_WIDTH = 650, MAX_HEIGHT = 650) {
    let resized_base64 = await new Promise((resolve) => {
        let img = new Image()
        img.src = base64Str
        img.onload = () => {
            let canvas = document.createElement('canvas')
            let width = img.width
            let height = img.height

            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width
                    width = MAX_WIDTH
                }
            } else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height
                    height = MAX_HEIGHT
                }
            }
            canvas.width = width
            canvas.height = height
            let ctx = canvas.getContext('2d')
            ctx.drawImage(img, 0, 0, width, height)
            resolve(canvas.toDataURL()) // this will return base64 image results after resize
        }
    });
    return resized_base64;
}


async function image_to_base64(file) {
    let result_base64 = await new Promise((resolve) => {
        let fileReader = new FileReader();
        fileReader.onload = (e) => resolve(fileReader.result);
        fileReader.onerror = (error) => {
            console.log(error)
            alert('An Error occurred please try again, File might be corrupt');
        };
        fileReader.readAsDataURL(file);
    });
    return result_base64;
}

async function process_image(file, min_image_size = 300) {
    const res = await image_to_base64(file);
    if (res) {
        const old_size = calc_image_size(res);
        if (old_size > min_image_size) {
            const resized = await reduce_image_file_size(res);
            const new_size = calc_image_size(resized)
            console.log('new_size=> ', new_size, 'KB');
            console.log('old_size=> ', old_size, 'KB');
            return resized;
        } else {
            console.log('image already small enough')
            return res;
        }

    } else {
        console.log('return err')
        return null;
    }
}

/*- NOTE: USE THIS JUST TO GET PROCESSED RESULTS -*/
// async function preview_image() {
//     const file = document.getElementById('file');
//     const image = await process_image(file.files[0]);
//     // console.log(image)
// }

/*- NOTE: USE THIS TO PREVIEW IMAGE IN HTML -*/
async function preview_image() {
    const file = document.getElementById('file');
    const res = await image_to_base64(file.files[0])
    if (res) {
        document.getElementById("old").src = res;

        const olds = calc_image_size(res)
        console.log('Old ize => ', olds, 'KB')

        const resized = await reduce_image_file_size(res);
        const news = calc_image_size(resized)
        console.log('new size => ', news, 'KB')
        document.getElementById("new").src = resized;
    } else {
        console.log('return err')
    }
}


function calc_image_size(image) {
    let y = 1;
    if (image.endsWith('==')) {
        y = 2
    }
    const x_size = (image.length * (3 / 4)) - y
    return Math.round(x_size / 1024)
}

function noImagePhoto() {
    let isExist = document.getElementById('captured_image').getAttribute('src');
    if (!isExist) {
        $(`.loading`).addClass('d-none');
        let title = "Gagal, Foto belum ada";
        let icon = "error";
        let iconColor = "#ff4d4d";
        let confirmButtonText = "<i class='fa fa-times-circle-o'></i> OK";
        let confirmButtonColor = "#399E6B";

        Swal.fire({
            title,
            icon,
            iconColor,
            confirmButtonText,
            confirmButtonColor
        }).then((result) => {
            $("#open").trigger('click');
        });
        return false;
    }
    return true;
}

