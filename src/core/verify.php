<?php
/*
 * Author: Dahir Muhammad Dahir
 * Date: 26-April-2020 5:44 PM
 * About: identification and verification
 * will be carried out in this file
 */

namespace fingerprint;

require_once("../core/helpers/helpers.php");
require_once("../core/querydb.php");

if(!empty($_POST["data"])) {
    $user_data = json_decode($_POST["data"]);
    $user_id = $user_data->id;
    //this is not necessarily index_finger it could be
    //any finger we wish to identify
    $pre_reg_fmd_string = $user_data->index_finger[0];
    $params=[
        "user_id" =>$user_id,
        'participant_id'=> $user_data->token
    ]; 
    
    //curl post
    $url = "https://transisi.space/adago79/api/adainduksi-v2/biometrik/verify-user?" . http_build_query($params);    
    $cs = curl_init(); 
    curl_setopt($cs, CURLOPT_URL, $url);
    curl_setopt($cs, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($cs); 
    $hand_data =json_decode($output, true); 

   
    if($hand_data['status']===404){
        echo json_encode("failed");
        // echo   json_encode($output);
        return false;
    };

   // echo $output;

  
    $enrolled_fingers = [
        "index_finger" => $hand_data['data'][0]['indexfinger'],
        "middle_finger" => $hand_data['data'][0]['middlefinger']
    ];

    $json_response = verify_fingerprint($pre_reg_fmd_string, $enrolled_fingers);
    $response = json_decode($json_response);


    if($response === "match"){
          //curl post
            $url = "https://transisi.space/adago79/api/adainduksi-v2/biometrik/change-status?" . http_build_query($params);    
            $cs = curl_init(); 
            curl_setopt($cs, CURLOPT_URL, $url);
            curl_setopt($cs, CURLOPT_RETURNTRANSFER, 1); 
            $out = curl_exec($cs); 
       
        echo   $output;
    }
    else{
        echo json_encode("failed");
      
    }


}

    
    //$hand_data = json_decode(getUserFmds($user_id));
   
    // $enrolled_fingers = [
    //     "index_finger" => $hand_data[0]->indexfinger,
    //     "middle_finger" => $hand_data[0]->middlefinger
    // ];

    // $json_response = verify_fingerprint($pre_reg_fmd_string, $enrolled_fingers);
    // $response = json_decode($json_response);

    // if($response === "match"){
    //     echo getUserDetails($user_id);
    // }
    // else{
    //     echo json_encode("failed");
    // }


else{
    echo "post request with 'data' field required";
}
