<?php
$inFolder = true;
$base11 = "http://localhost/FingerPrint/";
$topmenu = "biometric";
include 'component/topmenu.php';
//  echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; 
?>

<!-- Select2 -->
<link rel="stylesheet" href="<?= $base11 ?>assets/plugins/select2/dist/css/select2.min.css">
<!-- custom file -->
<link rel="stylesheet" href="<?= $base11 ?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css">
<link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.1/dist/sweetalert2.min.css" rel="stylesheet">
<link rel="stylesheet" href="./src/css/bootstrap.css">
<link rel="stylesheet" href="./src/css/custom.css">
<link rel="stylesheet" href="./src/css/simplePagination
.css">

<?php include 'component/header.php'; ?>

<style>
    .swal2-container.swal2-top,
    .swal2-container.swal2-center,
    .swal2-container.swal2-bottom {
        z-index: 999999999999 !important;
    }

    .swal2-styled.swal2-confirm {
        font-weight: 600;
        background-color: #399E6B;
    }

    .swal2-styled.swal2-cancel {
        font-weight: 600;
        color: #444;
    }

    .form-row {
        display: flex;
    }
</style>

</header>
<!-- end header -->

<div class="page-wrapper dashboard-wrapper">
    <div class="container">
        <?php include 'component/menu.php'; ?>


        <div class="row">
            <div class="col-md-12">
                <div class="card " style="min-height: 400px;">
                    <div class="box-data">
                        <form>
                            <div class="table-filter col">
                                <div class="row">
                                    <div class="col section-title mb-16">
                                        <h4>Daftar Biometric Sidik Jari Karyawan</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-box">
                                            <select id="location" class="form-input select2" data-allow-clear="true" data-placeholder="Pilih Lokasi Induksi">
                                                <option value=""></option>
                                                <option value="1">Adaro Indonesia</option>
                                                <option value="2">UT</option>
                                                <option value="3">Pama Persada</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-box form-date">
                                            <input type="text" id="date" class="form-input datepicker" placeholder="Tanggal Induksi" value="<?= date('d M Y') ?>" data-date-format="dd M yyyy">
                                            <img src="<?= $base11 ?>assets/img/icon-date.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-box">
                                            <select id="status" class="form-input select2" data-allow-clear="true" data-placeholder="Pilih Status Verifikasi">
                                                <option value=""></option>
                                                <option value="1">Belum</option>
                                                <option value="2">Sudah</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-box">
                                            <select id="department" class="form-input select2" data-allow-clear="true" data-placeholder="Pilih Departemen">
                                                <option value=""></option>
                                                <option value="1">HSE</option>
                                                <option value="2">Mining</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-box">
                                            <select id="company" class=" company form-input select2" data-allow-clear="true" data-placeholder="Pilih Perusahaan">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col col-search mr-12">
                                        <div class="search-wrap">
                                            <i class="fa fa-search"></i>
                                            <input type="text" id="search" placeholder="Cari Nama/NIK Peserta" name="q">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-box">
                                            <select id="" class="form-input perusahaan">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col" style="max-width: 50px;">
                                        <button class="btn-filter d-flex align-items-center" type="submit">
                                            <img src="<?= $base11 ?>assets/img/icon-filter.svg" alt="">
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col col-btn d-flex" style="gap:16px;">
                                        <a href="#" data-toggle="st-popup" data-target="#fingerprint-automatic-verification" class="btn" style="min-width: 190px;"><i class="fa fa-check-circle-o mr-8"></i>VERIFIKASI OTOMATIS</a>
                                        <!-- <a href="#" style="min-width: 140px;" id="createEnrollmentButton" class="btn" data-toggle="modal" data-target="#createEnrollment" onclick="beginEnrollment()">Create Enrollment</a>
                                        <a href="#" style="min-width: 120px;" id="verifyIdentityButton" class="btn" data-toggle="modal" data-target="#verifyIdentity" onclick="beginIdentification()">Verify Identity</a> -->
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="table-outer">
                            <table class="table table-custom">
                                <thead>
                                    <tr>
                                        <th style="width: 50px"></th>
                                        <th>Nama</th>
                                        <th>NIK</th>
                                        <th>Perusahaan</th>
                                        <th>Departemen</th>
                                        <th>Jadwal Induksi</th>
                                        <th>Lokasi Induksi</th>
                                        <th>Status Verifikasi</th>
                                        <th>Status Data Foto</th>
                                        <th>Status Data Sidik Jari</th>
                                    </tr>
                                </thead>
                                <tbody id="data-participant">
                                    <?php
                                    $data = [

                                        ['Ahmad Sahroni', '890678991', 'PT Adaro Indonesia', 'HSE', '20 Apr 2024', 'Gedung A', ['belum', 'high'], ['belum', 'high'], ['belum', 'high']],


                                    ];
                                    foreach ($data as $key => $val) :
                                    ?>
                                        <tr>
                                            <td colspan="10" class="text-center">
                                                Sedang Memuat Data
                                            </td>

                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="table-nav d-flex justify-content-between align-items-center">
                            <div class="table-numb">
                                <!-- Showing 1 to <?= count($data) ?> of <?= count($data) ?> entries -->
                            </div>
                            <ul id="pagination" class="table-pagination pagination"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <img class="footer-bg" src="<?= $base11 ?>assets/img/decoration-bottomleft.png" alt="">
    <div class="footer-wrap">
        <div class="container text-right">
            <img class="footer-logo" src="<?= $base11 ?>assets/img/adaro-logo-mining.png" alt="">
            <p>Copyright &copy; 2019 PT Adaro Indonesia.</p>
        </div>
    </div>
</footer>
</div>
<!-- /#wrapper -->

<?php include 'popup/fingerprint-input.php'; ?>
<?php include 'popup/fingerprint-verification.php'; ?>
<?php include 'popup/fingerprint-automatic-verification.php'; ?>
<?php include 'popup/take-photo.php'; ?>
<?php include 'popup/enroll-finger.php'; ?>
<?php include 'popup/verify-finger.php'; ?>
<?php include 'popup/detail.php'; ?>

<script type="text/javascript" src="<?= $base11 ?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?= $base11 ?>assets/js/adaro.min.js"></script>
<script type="text/javascript" src="<?= $base11 ?>assets/plugins/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?= $base11 ?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js">
</script>
<script type="text/javascript" src="<?= $base11 ?>assets/js/sandstrap-popup.js"></script>
<script src="<?= $base11 ?>src/js/bootstrap.bundle.js"></script>
<script src="<?= $base11 ?>src/js/es6-shim.js"></script>
<script src="<?= $base11 ?>src/js/websdk.client.bundle.min.js"></script>
<script src="<?= $base11 ?>src/js/fingerprint.sdk.min.js"></script>
<script src="<?= $base11 ?>src/js/custom.js"></script>
<script src="<?= $base11 ?>src/js/jquery.simplePagination.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.1/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>


<script>
    $(document).ready(function() {
        const isLogin = localStorage.getItem('auth');
        if (isLogin == null) {
            window.location.href = "index.php"
        }
    });
</script>


<!-- //script tabel -->
<script>

    $(document).ready(function() {
        //Initialize Select2 Elements
        $('.select2').select2();
        var url = "https://transisi.space/adago79/api/adainduksi-v2/";
        $(".company").select2({
            placeholder: 'Pilih perusahaan',
            allowClear: true,
            ajax: {
                url: url + 'data-master/company',
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        type: 2,
                        term: params.term || '',
                        page: params.page || 1
                    }
                },
                cache: true
            }
        });

        //Data picker
        $('.datepicker').datepicker({
            autoclose: true,
            setDate: new Date(),
        });



        //get data tabel 
        var url = "https://transisi.space/adago79/api/adainduksi-v2/";
        var page = 1;
        var current_page = 4;
        var total_page = 100;
        var is_ajax_fire = 0;
        var query = "?";


        manageData(query);
        $('.btn-filter').on('click', function(e) {
            e.preventDefault()
            manageData(query);
        })


        function filter() {

        }

        /* tampilkan data */
        function manageData(query) {

            let company = $('#company').val();
            if (company != '') {
                let q_company = "company=" + company + '&';
                query += q_company
            }

            let search = $('#search').val();
            if (search != '') {
                let q_search = "search=" + search + '&';
                query += q_search
            }

            let date = $('#date').val();
            if (date != '') {
                let q_date = "date=" + date + '&';
                query += q_date
            }


            $.ajax({
                dataType: 'json',
                url: url + 'participant' + query,
                data: {
                    page: page
                }
            }).done(function(data) {
                total_page = Math.ceil(data.meta.total / 10);
                current_page = page + current_page;

                $('#pagination').twbsPagination({
                    totalPages: total_page,
                    first: false,
                    last: false,
                    prev: '<span aria-hidden="true">&laquo;</span>',
                    next: '<span aria-hidden="true">&raquo;</span>',
                    visiblePages: current_page,
                    onPageClick: function(event, pageL) {
                        page = pageL;
                        if (is_ajax_fire != 0) {
                            getPageData(query);
                        }
                    }
                });

                manageRow(data.data);
                is_ajax_fire = 1;

            });
        }

        /* tampilkan  data */
        function getPageData(query) {

            let company = $('#company').val();
            if (company != '') {
                let q_company = "company=" + company + '&';
                query += q_company
            }

            let search = $('#search').val();
            if (search != '') {
                let q_search = "search=" + search + '&';
                query += q_search
            }

            let date = $('#date').val();
            if (date != '') {
                let q_date = "date=" + date + '&';
                query += q_date
            }

            $.ajax({
                dataType: 'json',
                url: url + 'participant' + query,
                data: {
                    page: page,
                    query: query
                }
            }).done(function(data) {
                manageRow(data.data);
            });
        }

        function manageRow(data) {
            var rows = '';
            $.each(data, function(key, value) { 
                rows = rows + '<tr>';
                rows = rows + `<td>

              
                                 <span class="action action-left" style="display: flex">
                               
                                 ${!value.is_take_finger ? `
                                <a href="#" title="Input Sidik Jari" id="createEnrollmentButton1" class="fingerprint-input"   data-toggle="modal" data-target="#createEnrollment1" onclick="beginEnrollment()" data-value="${value.id}">
                                <i class="mdi mdi-fingerprint"></i>
                                </a>`:`` }

                                <a href="#" title="Verifikasi/Take Foto " class="fingerprint-take-photo"  data-toggle="st-popup" data-target="#take-photo" data-value="${value.id}">
                                    <i class="fa fa-camera"></i>
                                 </a>

                                <a href="#" title="Verifikasi Data Sidik Jari By User"  id="verifyIdentityButton" class="fingerprint-verification" data-toggle="modal" data-target="#verifyIdentity"   data-value="${value.id}">
                                 <i class="fa fa-check-circle-o"></i>
                                </a>

                            
                                ${value.is_take_finger ? `
                                <a href="#" class="hapus-data-biometric" title="Clear Data Biometric"  data-value="${value.user_id}" >
                                    <i class="fa fa-times-circle-o"></i>
                                </a>`:''}

                                </span>
                              </td>`

                rows = rows + '<td>' + value.name + '</td>';
                rows = rows + '<td>' + value.nik + '</td>';
                rows = rows + '<td>' + value.company + '</td>';
                rows = rows + '<td>' + value.department + '</td>';
                rows = rows + '<td>' + value.implementation_date + '</td>';
                rows = rows + '<td>' + value.location + '</td>';

                rows = rows + `<td>  ${value.is_verification ? 
                                 '<span class="label label-finish">sudah</span>':'<span class="label label-high">belum</span>'} 
                              </td>`;
                rows = rows + `<td>  ${value.is_take_photo ? 
                                 '<span class="label label-finish">sudah</span>':'<span class="label label-high">belum</span>'} 
                              </td>`;
                rows = rows + `<td>  ${value.is_take_finger ? 
                                 '<span class="label label-finish">sudah</span>':'<span class="label label-high">belum</span>'} 
                              </td>`;

                rows = rows + '</td>';
                rows = rows + '</tr>';
            });
            $("#data-participant").html(rows);

            $('.fingerprint-input').on('click', function() {
                let participantId = $(this).data('value');
               
                $.ajax({
                url: url + 'participant/' + participantId,
                type: "get",
                dataType: "JSON",
                success: function(response) {
          
                // $('#userID').val(response.data.user_id)
                getDataParticipant(participantId)
                },
                error: function(response) {}

            });





            })

            $('.fingerprint-take-photo').on('click', function() {
                let getDataParticipant = $(this).data('value');
                getDataParticipant(participantId)
            })


            $('.fingerprint-verification').on('click', function() {
                let participantId = $(this).data('value');
                $('#userID').val('')


                  $.ajax({
                url: url + 'participant/' + participantId,
                type: "get",
                dataType: "JSON",
                success: function(response) {
                   
                //     $('#userIDVerify').val(response.data.user_id)
                //   $('#participantId').val(response.data.id)
            
                getDataParticipant(participantId)
                },
                error: function(response) {}

                });
                  
             
            
            })




            $('.hapus-data-biometric').click(function() {
                let userId = $(this).data('value');
               
            let title = "Apakah Anda Yakin Ingin Menghapus Data Rekam Biometric ?";
            let icon = "info";
            let iconColor = "#ebbd21";
            let confirmButtonText = "<i class='fa fa-check-circle-o'></i> Ya, Hapus";
            let confirmButtonColor = "#399E6B";
            let showCancelButton = true;
            let cancelButtonText = "BATAL";
            let cancelButtonColor = "#eee";

            Swal.fire({

                title,
                icon,
                iconColor,
                confirmButtonText,
                confirmButtonColor,
                showCancelButton,
                cancelButtonText,
                cancelButtonColor,

            }).then((result) => {

                console.log(result);
                let pesanBerhasil = "Data Rekam Biometric Berhasil Dihapus!";
                let kategori = "success";

                let pesanGagal = "Data Rekam Biometric Gagal Dihapus!";
                let kategoriGagal = "error";
               
                 

                    if(result.isConfirmed)
                    {
                        $.ajax({
                            url: url + 'participant/' + userId+'/delete',
                            type: "get",
                            dataType: "JSON",
                            success: function(response) {  
                                Swal.fire(pesanBerhasil, "", kategori);    
                                location.reload();
                            },
                            error: function(response) {
                                console.log(response);
                                Swal.fire(pesanGagal, "", kategoriGagal);
                                location.reload();  
                            }
                        });
                    }

            });
        })

        }




        //input finger print
        function getDataParticipant(participantId) {
            $.ajax({
                url: url + 'participant/' + participantId,
                type: "get",
                dataType: "JSON",
                success: function(response) {
                 
                   appendData(response)
                },
                error: function(response) {}

            });
        }


        function appendData(response) {

           
 
            $('.data-name').html(response.data.name)
            $('.data-nik').html(response.data.nik)
            $('.data-permit_number').html(response.data.minepermit_number)
            $('.data-company').html(response.data.company)
            $('.data-position').html(response.data.position)
            $('.data-department').html(response.data.department)
            $('.data-implementation-date').html(response.data.implementation_date)
            $('.data-location').html(response.data.location)

            if (response.data.is_take_finger == true) {
                $('.data-status-finger').html('<span class="label label-finish">Sudah</span>')
            } else {
                $('.data-status-finger').html('<span class="label label-high">Belum</span>')
            }

            if (response.data.is_verification == true) {
                $('.data-status-verification').html('<span class="label label-finish">Sudah</span>')
            } else {
                $('.data-status-verification').html('<span class="label label-high">Belum</span>')
            }

            if (response.data.is_take_photo == true) {
                $('.data-status-photo').html('<span class="label label-finish">Sudah</span>')
            } else {
                $('.data-status-photo').html('<span class="label label-high">Belum</span>')

            }
        }






    });
</script>

<!-- camera script -->
<script>
    $(document).ready(function() {
        //Initialize Select2 Elements
        $('.select2').select2();

        /* file upload */
        fileInput = $(document).find('.form-file__input');
        fileInput.each(function() {
            var uploadInput = $(this).find('.input-upload');
            var uploadLabel = $(this).find('.form-file__label');

            uploadInput.change(function() {
                var fileName = uploadInput.val().replace(/.*[\/\\]/, '');
                uploadLabel.text(fileName);
            });
        });

        $('#video').resize(function() {
            $('#cont').height($('#video').height());
            $('#cont').width($('#video').width());
            $('#control').height($('#video').height() * 0.1);
            $('#control').css('top', $('#video').height() * 0.9);
            $('#control').width($('#video').width());
            $('#control').show();
        });

        // make default photos
        var dataImage = localStorage.getItem('images');
        var img = $('<img" width="100%" height="100%" id="img_menu-item" style="height: 100%;width:100%">');
        jQuery('#pg_home_content').append(img)
        if (dataImage) {
            bannerImg = document.getElementById('result');
            bannerImg.src = dataImage;
            jQuery('#pg_home_content').append(bannerImg);
        } else {
            bannerImage = document.getElementById("img_menu-item");
            img[0].src = "<?= $base11 ?>assets/img/user.png";
        };

        // download photos from image src
        $("#btn-download").on('click', function(e) {
            e.preventDefault();
            const img = document.getElementById('result');
            img.src = $('#result').attr('src');
            const dir = img.src.replace(/^data:image\/jpeg/, "data:application/octet-stream");
            var a = $("<a>")
                .attr("href", dir)
                .attr("download", "img.jpeg")
                .appendTo("body");
            a[0].click();
            a.remove();
        });

        // check camera avaliable from broweser
        function openCam() {
            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator
                .mozGetUserMedia || navigator.oGetUserMedia || navigator.msGetUserMedia;
            if (navigator.getUserMedia) {
                navigator.getUserMedia({
                    video: true
                }, streamWebCam, throwError);
            }
        }

        function closeCam() {
            video.pause();
            try {
                video.srcObject = null;
            } catch (error) {
                video.src = null;
            }

            let track = strr.getTracks()[0];
            track.stop();
        }

        // make camera from video tag
        const video = document.getElementById('video');
        const canvas = document.getElementById('canvas');
        const context = canvas.getContext('2d');
        let strr;

        function streamWebCam(stream) {
            const mediaSource = new MediaSource(stream);
            try {
                video.srcObject = stream;
            } catch (error) {
                video.src = URL.createObjectURL(mediaSource);
            }
            video.play();
            strr = stream;
        }

        function throwError(e) {
            alert(e.name);
        }

        $('#open').click(function(event) {
            event.preventDefault();
            openCam();
            $('.camera-control').show();
            $("#camera").show();
            $("#profile-picture").hide();
            $('#vid').css('z-index', '30').show();
            $('#capture').css('z-index', '20').hide();
            $("#captured_image").hide();
        });

        $('#close').click(function(event) {
            event.preventDefault();
            closeCam();
        });
        $('.popup-close').click(function(event) {
            event.preventDefault();
            closeCam();
            $("#camera").hide();
            $("#profile-picture").show();
        });

        $('#snap').click(function(event) {
            event.preventDefault();
            canvas.width = 400;
            canvas.height = 500;
            context.scale(-1, 1);
            context.drawImage(video, 130, 5, 400, 470, -400, 0, 420, 500);
            resultb64 = canvas.toDataURL("image/jpeg");
            $("#captured_image").prop('src', resultb64).show();

            $('#vid').css('z-index', '20').hide();
            $('#capture').css('z-index', '30').hide();
        });

        // save photos from canvas then save to local storage
        $('#save').click(function(event) {
            event.preventDefault();
            localStorage.setItem("images", resultb64);
            let img = document.getElementById('result');
            img.src = resultb64;

            $('#vid').css('z-index', '30');
            $('#capture').css('z-index', '20');
            closeCam();
            // $('.popup-close').trigger('click');
            $("#camera").hide();
            $("#profile-picture").show();
            // window.sandstrapPopup.close()
        });

        $('#retake').click(function(event) {
            event.preventDefault();
            $('#vid').css('z-index', '30').show();
            $('#capture').css('z-index', '20').hide();
            $("#captured_image").hide();
        });
    });

    function confirmation() {
        confirm('Apakah ingin melakukan perubahan?');
    }
</script>


</body>

</html>