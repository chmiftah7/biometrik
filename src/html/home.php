<?php
include 'component/topmenu.php';
$base = getenv('BASE_URL');


?>

<!-- Select2 -->
<link rel="stylesheet" href="<?= $base ?>assets/plugins/select2/dist/css/select2.min.css">
<!-- custom file -->
<link rel="stylesheet" href="<?= $base ?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css">
<link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.1/dist/sweetalert2.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

<link rel="stylesheet" href="<?= $base ?>assets/plugins/daterangepicker-master/daterangepicker.css" />

<link rel="stylesheet" href="./src/css/bootstrap.css">
<link rel="stylesheet" href="./src/css/custom.css">
<link rel="stylesheet" href="./src/css/simplePagination.css">
<?php include 'component/header.php'; ?>

<style>
    .swal2-container.swal2-top,
    .swal2-container.swal2-center,
    .swal2-container.swal2-bottom {
        z-index: 999999999999 !important;
    }

    .swal2-styled.swal2-confirm {
        font-weight: 600;
        background-color: #399E6B;
    }

    .swal2-styled.swal2-cancel {
        font-weight: 600;
        color: #444;
    }

    .form-row {
        display: flex;
    }
</style>
<style>
    div.loading {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(16, 16, 16, 0.5);
        z-index: 9999;
    }

    @-webkit-keyframes uil-ring-anim {
        0% {
            -ms-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -webkit-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -ms-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-webkit-keyframes uil-ring-anim {
        0% {
            -ms-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -webkit-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -ms-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-moz-keyframes uil-ring-anim {
        0% {
            -ms-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -webkit-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -ms-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-ms-keyframes uil-ring-anim {
        0% {
            -ms-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -webkit-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -ms-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-moz-keyframes uil-ring-anim {
        0% {
            -ms-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -webkit-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -ms-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-webkit-keyframes uil-ring-anim {
        0% {
            -ms-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -webkit-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -ms-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-o-keyframes uil-ring-anim {
        0% {
            -ms-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -webkit-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -ms-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @keyframes uil-ring-anim {
        0% {
            -ms-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -webkit-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -ms-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    .uil-ring-css {
        margin: auto;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        width: 200px;
        height: 200px;
    }

    .uil-ring-css>div {
        position: absolute;
        display: block;
        width: 160px;
        height: 160px;
        top: 20px;
        left: 20px;
        border-radius: 80px;
        box-shadow: 0 6px 0 0 #ffffff;
        -ms-animation: uil-ring-anim 1s linear infinite;
        -moz-animation: uil-ring-anim 1s linear infinite;
        -webkit-animation: uil-ring-anim 1s linear infinite;
        -o-animation: uil-ring-anim 1s linear infinite;
        animation: uil-ring-anim 1s linear infinite;
    }
</style>
<style>
    .table-numb {
        color: #333333 !important;
    }

    #ajax-loading {
        position: fixed;
        top: 0;
        left: 0;
        z-index: 9999 !important;
        width: 100%;
        height: 100%;
        display: none;
        background: rgba(0, 0, 0, 0.6);
    }

    .ajax-spinner {
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .ajax-spinner .spinner {
        width: 40px;
        height: 40px;
        border: 4px #ddd solid;
        border-top: 4px #266141 solid;
        border-radius: 50%;
        animation: sp-anime 0.8s infinite linear;
    }

    @keyframes sp-anime {
        100% {
            transform: rotate(360deg);
        }
    }

    #ajax-loading {
        position: fixed;
        top: 0;
        left: 0;
        z-index: 9999 !important;
        width: 100%;
        height: 100%;
        display: none;
        background: rgba(0, 0, 0, 0.6);
    }

    .ajax-spinner {
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .ajax-spinner .spinner {
        width: 40px;
        height: 40px;
        border: 4px #ddd solid;
        border-top: 4px #266141 solid;
        border-radius: 50%;
        animation: sp-anime 0.8s infinite linear;
    }

    @keyframes sp-anime {
        100% {
            transform: rotate(360deg);
        }
    }
</style>

</header>
<!-- end header -->

<div class="page-wrapper dashboard-wrapper">
    <div class="container">
        <?php include 'component/menu.php'; ?>
        <div class="row">
            <div class="col-md-12">
                <div class="card " style="min-height: 400px;">
                    <div class="box-data">
                        <form>
                            <div class="table-filter col">
                                <!-- <div class="row">
                                    <div class="col section-title mb-16">
                                        <h4>Daftar Biometric Sidik Jari Karyawan</h4>
                                    </div>
                                </div> -->
                                <!-- <div class="row">
                                    <div class="col col-btn">
                                        <button type="button" class="btn" id="test">Test</h4>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col" style="min-width: 300px">
                                        <div class="form-box">
                                            <select id="location" class="form-input select2 location" data-allow-clear="true" data-placeholder="Pilih Lokasi Induksi">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col" style="min-width: 300px">
                                        <div class="form-box form-date">
                                            <input type="text" id="date" class="form-input daterange" placeholder="Tanggal Induksi" value="<?= date('d M Y') ?>" data-date-format="dd M yyyy" data-allow-clear="true">
                                            <img src="<?= $base ?>assets/img/icon-date.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="col" style="min-width: 300px">
                                        <div class="form-box">
                                            <select id="status" class="form-input select2" data-allow-clear="true" data-placeholder="Status Verifikasi Kehadiran">
                                                <option value=""></option>
                                                <option value="0">Belum Hadir</option>
                                                <option value="1">Hadir</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col" style="min-width: 300px">
                                        <div class="form-box">
                                            <select id="status_foto" class="form-input select2 status-finger" data-allow-clear="true" data-placeholder="Pilih Status Data Foto">
                                                <option value=""></option>
                                                <option value="0">Belum Rekam/Upload Foto</option>
                                                <option value="1">Lengkap</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col" style="min-width: 300px">
                                        <div class="form-box">
                                            <select id="status_finger" class="form-input select2 status-foto" data-allow-clear="true" data-placeholder="Pilih Status Data Sidik Jari">
                                                <option value=""></option>
                                                <option value="0">Belum Rekam Sidik Jari</option>
                                                <option value="1">Lengkap</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col" style="min-width: 300px">
                                        <div class="form-box">
                                            <select id="department" class="form-input select2 department" data-allow-clear="true" data-placeholder="Pilih Departemen">
                                                <option value=""></option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col" style="min-width: 300px">
                                        <div class="form-box">
                                            <select id="company" class=" company form-input select2" data-allow-clear="true" data-placeholder="Pilih Perusahaan">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col col-search mr-12">
                                        <div class="search-wrap">
                                            <i class="fa fa-search"></i>
                                            <input type="text" id="search" placeholder="Cari Nama/NIK Peserta" name="q">
                                        </div>
                                    </div>
                                    <div class="col" style="max-width: 50px;">
                                        <button class="btn-filter d-flex align-items-center" type="submit">
                                            <img src="<?= $base ?>assets/img/icon-filter.svg" alt="">
                                        </button>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                    <div class="col col-btn d-flex" style="gap:16px;">
                                        <a href="#" data-toggle="st-popup" data-target="#fingerprint-automatic-verification" class="btn" style="min-width: 190px;"><i class="fa fa-check-circle-o mr-8"></i>VERIFIKASI OTOMATIS</a>
                                        <a href="#" style="min-width: 140px;" id="createEnrollmentButton" class="btn" data-toggle="modal" data-target="#createEnrollment" onclick="beginEnrollment()">Create Enrollment</a>
                                        <a href="#" style="min-width: 120px;" id="verifyIdentityButton" class="btn" data-toggle="modal" data-target="#verifyIdentity" onclick="beginIdentification()">Verify Identity</a>
                                    </div>
                                </div> -->
                            </div>
                        </form>

                        <div class="table-outer">
                            <table class="table table-custom">
                                <thead>
                                    <tr>
                                        <th style="width: 50px"></th>
                                        <th>Nama</th>
                                        <th>NIK</th>
                                        <th>Perusahaan</th>
                                        <th>Departemen</th>
                                        <th>Jadwal Induksi</th>
                                        <th>Lokasi Induksi</th>
                                        <th>Status Data Foto</th>
                                        <th>Status Data Sidik Jari</th>
                                        <th>Verifikasi Kehadiran</th>
                                    </tr>
                                </thead>
                                <tbody id="data-participant">
                                    <?php
                                    $data = [
                                        [],
                                    ];
                                    foreach ($data as $key => $val) :
                                    ?>
                                        <tr>
                                            <td colspan="10" class="text-center">
                                                <div class="loader">
                                                    <span class="loader-text">Sedang Memuat Data</span>
                                                </div>
                                            </td>

                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="table-nav d-flex justify-content-between align-items-center flex-wrap">
                            <div class="table-numb">
                                Showing <span class="page-from"></span> to <span class="page-to"></span> of <span class="page-total"></span> entries
                            </div>
                            <ul id="pagination" class="table-pagination pagination"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="ajax-loading">
        <div class="ajax-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <div class="loading d-none">
        <div class='uil-ring-css' style='transform:scale(0.50);'>
            <div></div>
        </div>
    </div>

<footer class="footer">
    <img class="footer-bg" src="<?= $base ?>assets/img/decoration-bottomleft.png" alt="">
    <div class="footer-wrap">
        <div class="container text-right">
            <img class="footer-logo" src="<?= $base ?>assets/img/adaro-logo-mining.png" alt="">
            <p>Copyright &copy; 2019 PT Adaro Indonesia.</p>
        </div>
    </div>
</footer>
</div>
<!-- /#wrapper -->

<?php include 'popup/fingerprint-input.php'; ?>
<?php include 'popup/fingerprint-verification.php'; ?>
<?php include 'popup/fingerprint-automatic-verification.php'; ?>
<?php include 'popup/take-photo.php'; ?>
<?php include 'popup/enroll-finger.php'; ?>
<?php include 'popup/verify-finger.php'; ?>
<?php include 'popup/detail.php'; ?>

<script type="text/javascript" src="<?= $base ?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?= $base ?>assets/js/adaro.min.js"></script>
<script type="text/javascript" src="<?= $base ?>assets/plugins/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?= $base ?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= $base ?>assets/plugins/daterangepicker-master/moment.min.js"></script>
<script type="text/javascript" src="<?= $base ?>assets/plugins/daterangepicker-master/daterangepicker.js"></script>
<script type="text/javascript" src="<?= $base ?>assets/js/sandstrap-popup.js"></script>
<script src="<?= $base ?>src/js/bootstrap.bundle.js"></script>
<script src="<?= $base ?>src/js/es6-shim.js"></script>
<script src="<?= $base ?>src/js/websdk.client.bundle.min.js"></script>
<script src="<?= $base ?>src/js/fingerprint.sdk.min.js"></script>
<script src="<?= $base ?>src/js/custom.js"></script>
<script src="<?= $base ?>src/js/jquery.simplePagination.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.1/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>
<script src="<?= $base ?>src/js/env.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
    $(document).ready(function() {
        $('#test').on('click', function() {
            console.log('test');

        })

        getLocalStorage('auth')
        // Fungsi untuk mengambil data dari local storage dengan key
        function getLocalStorage(key) {
            const itemStr = localStorage.getItem(key);
            // Jika item tidak ditemukan atau sudah kedaluwarsa, kembalikan null
            if (!itemStr) {
                window.location.href = "index.php"
            }
            const item = JSON.parse(itemStr);
            const now = new Date();
            // Periksa apakah item telah kedaluwarsa
            if (now.getTime() > item.expiration) {
                // Jika telah kedaluwarsa, hapus item dari local storage
                localStorage.removeItem(key);
                window.location.href = "index.php"
            }
            return item.value;
        }

        // see current_page using page event
        // var current_page;
        // $('#pagination').on('page',function(event, page) {
            // current_page = page;
            // console.log('current_page', current_page);
        // });
    });

    window.addEventListener('beforeunload', function () {
        // console.log(1);
        localStorage.removeItem('currentPage');
        localStorage.removeItem('updateData');
        localStorage.removeItem('firstLoad');
    })
</script>


<!-- //script tabel -->
<script src="<?= $base ?>src/js/table.js"></script>
<!-- camera script -->
<script src="<?= $base ?>src/js/new-camera.js"></script>

</body>

</html>