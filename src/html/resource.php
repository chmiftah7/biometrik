<script>
    $(document).ready(function() {

        console.log('aa');
        $(".btn-login").click(function(e) {
            e.preventDefault();
            var email = $(".email").val();
            var password = $(".password").val();
            var token = $("meta[name='csrf-token']").attr("content");

            if (email.length == "") {
                $('.error-email').html('username wajib diisi')

            } else if (password.length == "") {
                $('.error-password').html('password wajib diisi')

            } else {

                $.ajax({

                    url: "http://localhost:8000/api/adainduksi-v2/login",
                    type: "post",
                    dataType: "JSON",
                    data: {
                        "email": email,
                        "password": password,
                    },

                    success: function(response) {

                        if (response.success) {
                            console.log(response);

                            const keyToRemove = "auth";
                            removeItemFromLocalStorage(keyToRemove);

                            const expirationHours = 2; // Waktu kedaluwarsa dalam jam
                            setLocalStorageWithTimer("auth", response.data, expirationHours);


                            toastr.success('Berhasil Login');
                            window.location.href = "home.php"


                        } else {
                            console.log(response);
                        }

                    },

                    error: function(response) {
                        let message = response.responseJSON.message;

                        toastr.error(message);

                    }

                });

            }

        });

    });




    // Fungsi untuk menyimpan data ke local storage dengan key dan value
    function setLocalStorageWithTimer(key, value, expiration) {
        const now = new Date();
        const item = {
            value: value,
            expiration: now.getTime() + expiration * 60 * 60 * 1000 // Menambahkan waktu kedaluwarsa (dalam milidetik)
        };
        localStorage.setItem(key, JSON.stringify(item));
    }

    // Fungsi untuk mengambil data dari local storage dengan key
    function getLocalStorage(key) {
        const itemStr = localStorage.getItem(key);
        // Jika item tidak ditemukan atau sudah kedaluwarsa, kembalikan null
        if (!itemStr) {
            return null;
        }
        const item = JSON.parse(itemStr);
        const now = new Date();
        // Periksa apakah item telah kedaluwarsa
        if (now.getTime() > item.expiration) {
            // Jika telah kedaluwarsa, hapus item dari local storage
            localStorage.removeItem(key);
            return null;
        }
        return item.value;
    }


    // Menghapus item dari local storage dengan key tertentu
    function removeItemFromLocalStorage(key) {
        localStorage.removeItem(key);
    }

    // Contoh penggunaan
</script>