<div class="st-popup-wrap filter-popup" id="detail" style="display: none;">
    <div class="container">
        <div class="popup-box" style="max-width: 700px !important;">
            <span class="popup-close"></span>
            <h3 class="popup-header text-left" style="font-size: 25px;">Detail Data Biometric Sidik Jari</h3>

            <div class="popup-body">
                <table class="table-stripped-border table-stripped">
                    <tr>
                        <td>Nama</td>
                        <td class="data-name"></td>
                    </tr>
                    <tr>
                        <td>NIK</td>
                        <td class="data-nik"></td>
                    </tr>
                    <tr>
                        <td>Nomor Mine Permit</td>
                        <td class="data-permit_number"></td>
                    </tr>
                    <tr>
                        <td>Perusahaan</td>
                        <td class="data-company"></td>
                    </tr>
                    <tr>
                        <td>Jabatan</td>
                        <td class="data-position"></td>
                    </tr>
                    <tr>
                        <td>Departemen</td>
                        <td class="data-department"></td>
                    </tr>
                    <tr>
                        <td>Jadwal Induksi</td>
                        <td class="data-implementation-date-hours"></td>
                    </tr>
                    <tr>
                        <td>Lokasi Induksi</td>
                        <td class="data-location"></td>
                    </tr>
                    <tr>
                        <td>Status Data Sidik Jari</td>
                        <td class="data-status-finger"></td>
                    </tr>
                    <tr>
                        <td>Status Data Foto</td>
                        <td class="data-status-photo"></td>
                    </tr>
                    <tr>
                        <td>Status Verifikasi</td>
                        <td class="data-status-verification"></td>
                    </tr>
                </table>

                <div class="form-submit text-right">
                    <a href="javascript:void(0);" onclick="window.sandstrapPopup.close()" class="btn btn-outline btn-outline--default">CLOSE</a>
                </div>
            </div>
        </div>
    </div>

    <div id="ajax-loading">
        <div class="ajax-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <div class="loading d-none">
        <div class='uil-ring-css' style='transform:scale(0.79);'>
            <div></div>
        </div>
    </div>

</div>