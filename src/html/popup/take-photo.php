<div class="st-popup-wrap filter-popup" id="take-photo" style="display: none;">
    <div class="container">
        <div class="popup-box" style="max-width: 900px !important;">
            <span class="popup-close"></span>
            <h3 class="popup-header text-left ">Data Biometric Foto Karyawan</h3>

            <div class="popup-body">
                <form action="">


                    <div id="sebab-akibat-repeat" class="vertical-form repeatform">
                        <!-- <div class="vertical-form__title">
                            Rekam Data Sidik Jari
                        </div> -->

                        <div class="p-16">

                            <div class="form-date text-center" id="profile-picture">
                                <img src="" alt="profil" style="width: 200px;" id="foto_permit">
                                <div class="d-flex justify-content-center">
                                    <a href="#" class="mine-permit-profile" id="open">
                                        <i class="fa fa-camera" style="left: 50%; transform: translateX(-50%)"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="camera" style="display: none;">
                                <div class="center">
                                    <div id="cont">
                                        <div id="vid" class="son" style="height: 300px !important;">
                                            <video id="video"></video>
                                            <div class="frame">
                                                <img src="<?= $base ?>assets/img/frame-photos.png" alt="frame-photos" class="frame-photos">
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-center mt-24 pt-16">
                                            <img src="<?= $base ?>assets/img/user-icon.png" alt="" id="captured_image" style="width: 240px; display: none">
                                        </div>
                                        <div id="capture" class="son">
                                            <canvas id="canvas"></canvas>
                                        </div>
                                    </div>
                                </div>

                                <div class="popup-footer">

                                    <div class="form-submit text-center mb-24">
                                        <div class="row">
                                            <div class="col">
                                                <button type="button" class="btn" id="snap"><i class="fa fa-camera-retro"></i> AMBIL FOTO</button>
                                                <button type="button" class="btn" id="frontBackCamera"><i class="mdi mdi-camera-party-mode"></i> KAMERA DEPAN</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <label for="imgInp" id="uploadPhoto" class="btn my-8" style="z-index: 100;">
                                                    <i class="fa fa-camera-retro"></i>
                                                    <span>UPLOAD FOTO</span>
                                                    <input accept="image/*" type='file' id="imgInp" class="col btn mr-16" style="z-index: 100; display:none" />
                                                </label>
                                                <!-- <button type="button" class="btn mr-16" id="savee" style="width: 195px;"><i class="fa fa-camera-retro"></i> UPLOAD FOTO</button> -->
                                                <button type="button" id="verifikasi" class="btn"><i class="fa fa-check-circle-o"></i> VERIFIKASI FOTO</button>
                                                <button type="button" id="retake" class="btn"> <i class="fa fa-refresh"></i> HAPUS</button>
                                                <!-- <input type="button" class="btn" id="kameraDepanBelakang" value="Back" /> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <table class="table-stripped-border table-stripped">

                        <input type="hidden" name="" class="participant-id" id="">

                        <input type="hidden" name="" class="participant-user-id" id="">
                        <tr>
                            <td>Nama</td>
                            <td class="data-name"></td>
                        </tr>
                        <tr>
                            <td>NIK</td>
                            <td class="data-nik"></td>
                        </tr>
                        <tr>
                            <td>Nomor Mine Permit</td>
                            <td class="data-permit_number"></td>
                        </tr>
                        <tr>
                            <td>Perusahaan</td>
                            <td class="data-company"></td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td class="data-position"></td>
                        </tr>
                        <tr>
                            <td>Departemen</td>
                            <td class="data-department"></td>
                        </tr>
                        <tr>
                            <td>Jadwal Induksi</td>
                            <td class="data-implementation-date-hours"></td>
                        </tr>
                        <tr>
                            <td>Lokasi Induksi</td>
                            <td class="data-location"></td>
                        </tr>
                        <tr>
                            <td>Status Data Sidik Jari</td>
                            <td class="data-status-finger"></td>
                        </tr>
                        <tr>
                            <td>Status Data Foto</td>
                            <td class="data-status-photo"></td>
                        </tr>
                        <!-- <tr>
                            <td>Status Verifikasi</td>
                            <td class="data-status-verification"></td>
                        </tr> -->
                    </table>

                    <!-- photo input -->
                    <input type="hidden" name="image" id="image-photo">

                    <div class="form-submit text-right">
                        <button type="button" onclick="window.sandstrapPopup.close()" class="btn btn-outline btn-outline--default">BATAL</button>
                        <button type="button" id="save" class="btn d-none"><i class="fa fa-save mr-8"></i>SIMPAN</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- <div id="ajax-loading">
        <div class="ajax-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <div class="loading d-none">
        <div class='uil-ring-css' style='transform:scale(0.79);'>
            <div></div>
        </div>
    </div> -->
</div>