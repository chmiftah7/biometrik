<section>
    <!--Create Enrolment Section-->
    <div class="modal fade" id="createEnrollment1" data-backdrop="static" tabindex="-1" aria-labelledby="createEnrollmentTitle" aria-hidden="true">
        <div class="container">
            <div class="popup-box">


                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="section-title small mb-0">
                                <h4 d="createEnrollmentTitle">Input Data Biometric Sidik Jari</h4>
                            </div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clearModal()">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>


                        <div class="modal-body">
                            <div id="sebab-akibat-repeat" class="vertical-form repeatform">
                                <div class="vertical-form__title" style="text-transform: capitalize;">
                                    Rekam Data Sidik Jari

                                </div>
                                <form action="#" onsubmit="return false" class="p-16">
                                    <div id="enrollmentStatusField" class="text-center">
                                        <!--Enrollment Status will be displayed Here-->
                                    </div>
                                    <div class="form-row mt-3">
                                        <div class="col mb-3 mb-md-0 text-center">
                                            <div class="form-box">
                                                <!-- <label for="enrollReaderSelect" class="">Choose Fingerprint Reader</label> -->
                                                <select name="readerSelect" id="enrollReaderSelect" class="form-control select2 form-input" onclick="beginEnrollment()">
                                                    <option selected>Pilih Alat Rekam Sidik Jari</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row mt-2 d-none">
                                        <div class="col mb-3 mb-md-0 text-center">
                                            <input id="userID" type="text" name="userID" class="form-control data-user-id" required>
                                        </div>
                                        <div class="col mb-3 mb-md-0 text-center d-none">
                                            <input id="token" name="token" type="text" class="form-control" value="12345">
                                        </div>
                                    </div>
                                    <div class="form-row mt-1">
                                        <div class="col card-title text-center mb-16">
                                            <h5>Rekam Jari Telunjuk</h5>
                                        </div>
                                    </div>
                                    <div id="indexFingers" class="form-row justify-content-center">
                                        <div id="indexfinger1" class="col mb-3 mb-md-0 text-center">
                                            <span class="icon icon-indexfinger-not-enrolled" title="not_enrolled"></span>
                                        </div>
                                        <div id="indexfinger2" class="col mb-3 mb-md-0 text-center">
                                            <span class="icon icon-indexfinger-not-enrolled" title="not_enrolled"></span>
                                        </div>
                                        <div id="indexfinger3" class="col mb-3 mb-md-0 text-center">
                                            <span class="icon icon-indexfinger-not-enrolled" title="not_enrolled"></span>
                                        </div>
                                        <div id="indexfinger4" class="col mb-3 mb-md-0 text-center">
                                            <span class="icon icon-indexfinger-not-enrolled" title="not_enrolled"></span>
                                        </div>
                                    </div>
                                    <div class="form-row mt-1">
                                        <div class="col card-title text-center mb-16">
                                            <h5>Rekam Jari Tengah</h5>
                                        </div>
                                    </div>
                                    <div id="middleFingers" class="form-row justify-content-center">
                                        <div id="middleFinger1" class="col mb-3 mb-md-0 text-center">
                                            <span class="icon icon-middlefinger-not-enrolled" title="not_enrolled"></span>
                                        </div>
                                        <div id="middleFinger2" class="col mb-3 mb-md-0 text-center">
                                            <span class="icon icon-middlefinger-not-enrolled" title="not_enrolled"></span>
                                        </div>
                                        <div id="middleFinger3" class="col mb-3 mb-md-0 text-center">
                                            <span class="icon icon-middlefinger-not-enrolled" title="not_enrolled"></span>
                                        </div>
                                        <div id="middleFinger4" class="col mb-3 mb-md-0 text-center" value="true">
                                            <span class="icon icon-middlefinger-not-enrolled" title="not_enrolled"></span>
                                        </div>
                                    </div>
                                    <div class="form-submit text-center">
                                        <button class="btn btn-sm mb-4 mr-16" type="submit" onclick="beginCapture()"><i class="mdi mdi-fingerprint"></i> Rekam Sidik Jari</button>
                                        <button class="btn btn-sm mb-4 mr-16" type="submit" onclick="serverEnroll()"> <i class="fa fa-save"></i> Simpan</button>
                                        <button class="btn mb-4" type="button" onclick="clearCapture()"><i class="fa fa-refresh"></i> Hapus</button>
                                    </div>


                            </div>

                            <table class="table-stripped-border table-stripped">
                                <tr>
                                    <td>Nama</td>
                                    <td class="data-name"></td>
                                </tr>

                                <tr>
                                    <td>NIK</td>
                                    <td class="data-nik"></td>
                                </tr>
                                <tr>
                                    <td>Nomor Mine Permit</td>
                                    <td class="data-permit_number"></td>
                                </tr>
                                <tr>
                                    <td>Perusahaan</td>
                                    <td class="data-company"></td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td class="data-position"></td>
                                </tr>
                                <tr>
                                    <td>Departemen</td>
                                    <td class="data-department"></td>
                                </tr>
                                <tr>
                                    <td>Jadwal Induksi</td>
                                    <td class="data-implementation-date-hours"></td>
                                </tr>
                                <tr>
                                    <td>Lokasi Induksi</td>
                                    <td class="data-location"></td>
                                </tr>
                                <tr>
                                    <td>Status Data Sidik Jari</td>
                                    <td class="data-status-finger"></td>
                                </tr>
                                <tr>
                                    <td>Status Data Foto</td>
                                    <td class="data-status-photo"></td>
                                </tr>
                            </table>
                            </form>

                            <div class="form-submit text-right">
                                <button class="btn btn-outline btn-outline--default" type="button" data-dismiss="modal" onclick="clearCapture()">BATAL</button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!-- <div id="ajax-loading">
            <div class="ajax-spinner">
                <span class="spinner"></span>
            </div>
        </div>
        <div class="loading d-none">
            <div class='uil-ring-css' style='transform:scale(0.79);'>
                <div></div>
            </div>
        </div> -->
    </div>

</section>