<section>
    <!--Verify Identity Section-->
    <div id="verifyIdentity" class="modal fade" data-backdrop="static" tabindex="-1" aria-labelledby="verifyIdentityTitle" aria-hidden="true">

        <div class="container">
            <div class="popup-box">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="section-title small mb-0">
                                <h4 id="createEnrollmentTitle">Verifikasi Data Biometric Sidik Jari</h4>
                            </div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clearModal()">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body p-16">
                            <div id="sebab-akibat-repeat" class="vertical-form repeatform">
                                <div class="vertical-form__title" style="text-transform: capitalize;">
                                    Rekam Data Sidik Jari

                                </div>
                                <form action="#" onsubmit="return false" class="p-16">
                                    <div id="verifyIdentityStatusField" class="text-center">
                                        <!--verifyIdentity Status will be displayed Here-->
                                    </div>
                                    <div class="form-row mt-3">
                                        <div class="col mb-3 mb-md-0 text-center">
                                            <div class="form-box mb-0" onclick="beginIdentification()">
                                                <!-- <label for="verifyReaderSelect" class="">Choose Fingerprint Reader</label> -->
                                                <select name="readerSelect2" id="verifyReaderSelect" class="form-control select2">
                                                    <option selected>Pilih Alat Rekam Sidik Jari</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row mt-4 d-none">
                                        <div class="col mb-md-0 text-center">
                                            <label for="userIDVerify" class=" m-0">Specify UserID</label>
                                            <input type="text" id="userIDVerify" name="userIDVerify" class="form-control mt-1 " value="">

                                        </div>
                                        <div class="col mb-3 mb-md-0 text-center ">
                                            <input id="participantId" name="participantId" type="text" class="form-control" value="">
                                        </div>
                                    </div>

                                    <div class="form-row mt-3">
                                        <div class="col card-title text-center mb-8">
                                            <h5>Rekam Sidik Jari</h5>
                                        </div>
                                    </div>
                                    <div id="verificationFingers" class="form-row justify-content-center">
                                        <div id="verificationFinger" class="col mb-md-0 text-center">
                                            <span class="icon icon-indexfinger-not-enrolled" title="not_enrolled"></span>
                                        </div>
                                    </div>
                                    <div class="form-row mt-3" id="userDetails">
                                        <!--this is where user details will be displayed-->
                                    </div>
                                    <!-- <div class="d-flex  m-3 mt-md-5 justify-content-center " style="gap: 8px;">
                                        <div class="">
                                            <button class="btn py-1" type="submit" onclick="captureForIdentify()"><i class="fa fa-camera-retro"></i> Start Capture</button>
                                        </div>
                                        <div class="">
                                            <button class="btn py-1" type="submit" onclick="serverIdentify()"><i class="fa fa-check-circle-o"></i> VERIFIKASI</button>
                                        </div>
                                        <div class="">
                                            <button class="btn py-1" type="button" onclick="clearCaptureIdentity()"><i class="fa fa-refresh"></i> CLEAR</button>
                                        </div>
                                    </div> -->
                                    <div class="form-submit text-center">
                                        <button class="btn btn-sm mb-4 mr-16" type="submit" onclick="captureForIdentify()"><i class="mdi mdi-fingerprint"></i> Rekam Sidik Jari</button>
                                        <button class="btn btn-sm mb-4 mr-16" type="submit" onclick="serverIdentify()"> <i class="fa fa-check-circle-o"></i> Verifikasi</button>
                                        <button class="btn mb-4" type="button" onclick="clearCaptureIdentity()"><i class="fa fa-refresh"></i> Hapus</button>
                                    </div>
                            </div>









                            <table class="table-stripped-border table-stripped">
                                <tr>
                                    <td>Nama</td>
                                    <td class="data-name"></td>
                                </tr>

                                <tr>
                                    <td>NIK</td>
                                    <td class="data-nik"></td>
                                </tr>
                                <tr>
                                    <td>Nomor Mine Permit</td>
                                    <td class="data-permit_number"></td>
                                </tr>
                                <tr>
                                    <td>Perusahaan</td>
                                    <td class="data-company"></td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td class="data-position"></td>
                                </tr>
                                <tr>
                                    <td>Departemen</td>
                                    <td class="data-department"></td>
                                </tr>
                                <tr>
                                    <td>Jadwal Induksi</td>
                                    <td class="data-implementation-date-hours"></td>
                                </tr>
                                <tr>
                                    <td>Lokasi Induksi</td>
                                    <td class="data-location"></td>
                                </tr>
                                <tr>
                                    <td>Status Data Sidik Jari</td>
                                    <td class="data-status-finger"></td>
                                </tr>
                                <tr>
                                    <td>Status Data Foto</td>
                                    <td class="data-status-photo"></td>
                                </tr>
                            </table>
                            </form>

                            <!-- <div class="form-submit text-right">
                                <button class="btn btn-outline btn-outline--default" type="button" data-dismiss="modal" onclick="clearCapture()">BATAL</button>
                            </div> -->
                        </div>
                        <div class="modal-footer">
                            <div class="form-row">
                                <div class="col">
                                    <button class="btn btn-outline btn-outline--default" type="button" data-dismiss="modal" onclick="clearModal()">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div id="ajax-loading">
            <div class="ajax-spinner">
                <span class="spinner"></span>
            </div>
        </div>
        <div class="loading d-none">
            <div class='uil-ring-css' style='transform:scale(0.79);'>
                <div></div>
            </div>
        </div> -->
    </div>

</section>