<div class="st-popup-wrap filter-popup" id="fingerprint-verification" style="display: none;">
    <div class="container">
        <div class="popup-box" style="max-width: 1200px !important;">
            <span class="popup-close"></span>
            <h3 class="popup-header text-left" style="font-size: 25px;">Verifikasi Data Biometric Sidik Jari</h3>

            <div class="popup-body">
                <form action="">

                    <div id="sebab-akibat-repeat" class="vertical-form repeatform">
                        <div class="vertical-form__title">
                            Rekam Data Sidik Jari
                            <!-- <a href="#" class="repeat-add"><i class="fa fa-plus-square"></i></a> -->
                        </div>

                        <div class="p-16">
                            <div class="form-row mb-24">
                                <div class="col card-title text-center mb-8">
                                    <h5>Capture Finger</h5>
                                </div>
                            </div>

                            <div id="indexFingers" class="form-row justify-content-center mb-32" style="gap: 16px;">
                                <div class="icon icon-indexfinger-not-enrolled" title="not_enrolled"></div>
                                <div class="icon icon-middlefinger-not-enrolled" title="not_enrolled"></div>
                                <!-- <div id="indexfinger1" class="col mb-3 mb-md-0 text-center">
                                </div>
                                <div id="middleFinger1" class="col mb-3 mb-md-0 text-center">
                                </div> -->
                            </div>
                        </div>

                        <div class="form-submit text-center mb-24">
                            <button type="button" class="btn mr-16" style="width: 195px;"><i class="fa fa-camera-retro"></i> START CAPTURE INDEX FINGER</button>
                            <button type="button" class="btn mr-16" style="width: 195px;"><i class="fa fa-camera-retro"></i> START CAPTURE MIDDLE FINGER</button>
                            <button type="button" onclick="modalStatusAlert('proses-verifikasi-foto', 'failed')" class="btn mr-16"><i class="fa fa-check-circle-o"></i> VERIFIKASI</button>
                            <button type="button" class="btn"><i class="fa fa-refresh"></i> CLEAR</button>


                            <!-- <button type="button" id="capture" class="btn mr-16"><i class="fa fa-camera-retro"></i> START CAPTURE</button>
                            <button type="button" id="simpan" class="btn mr-16"><i class="fa fa-save"></i> SIMPAN</button>
                            <button type="button" id="clear" class="btn"><i class="fa fa-refresh"></i> CLEAR</button> -->
                        </div>
                    </div>


                    <table class="table-stripped-border table-stripped">
                        <tr>
                            <td>Nama</td>
                            <td class="data-name"></td>
                        </tr>
                        <tr>
                            <td>NIK</td>
                            <td class="data-nik"></td>
                        </tr>
                        <tr>
                            <td>Nomor Mine Permit</td>
                            <td class="data-permit_number"></td>
                        </tr>
                        <tr>
                            <td>Perusahaan</td>
                            <td class="data-company"></td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td class="data-position"></td>
                        </tr>
                        <tr>
                            <td>Departemen</td>
                            <td class="data-department"></td>
                        </tr>
                        <tr>
                            <td>Jadwal Induksi</td>
                            <td class="data-implementation-date"></td>
                        </tr>
                        <tr>
                            <td>Lokasi Induksi</td>
                            <td class="data-location"></td>
                        </tr>
                        <tr>
                            <td>Status Data Sidik Jari</td>
                            <td class="data-status-finger"></td>
                        </tr>
                        <tr>
                            <td>Status Data Foto</td>
                            <td class="data-status-photo"></td>
                        </tr>
                    </table>

                    <!-- photo input -->
                    <!-- <input type="hidden" name="image" id="image-photo"> -->

                    <div class="form-submit text-right">
                        <a href="javascript:void(0);" onclick="window.sandstrapPopup.close()" class="btn btn-outline btn-outline--default">BATAL</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>