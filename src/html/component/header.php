  <!-- custom adaro -->
  <?php
    $base = getenv('BASE_URL');
    ?>
  <link rel="stylesheet" href="<?= $base ?>assets/css/adaro.css?v=<?= mt_rand() / mt_getrandmax(); ?>">

  <!-- font -->
  <link rel="stylesheet" href="<?= $base ?>assets/css/font.min.css?v=<?= mt_rand() / mt_getrandmax(); ?>">



  <script>
      document.addEventListener("DOMContentLoaded", function() {


          var $sidemenu = document.querySelector(".user-sidemenu");
          var sidebarToggle = document.getElementById("sidebar-toggle");

          var $screen = $(window).width();
          // set sessionStorage
          if (window.innerWidth > 767) !sessionStorage.sidebar && sessionStorage.setItem("sidebar", "on");
          else !sessionStorage.sidebar && sessionStorage.setItem("sidebar", "off");

          if (sessionStorage.sidebar == "on") {
              $("#sidebar-toggle").prop("checked", true).trigger("change");
              $(".user-sidemenu, .user-sidechart").removeClass("sidebar-toggle__close");
          } else {
              $("#sidebar-toggle").prop("checked", false).trigger("change");
              $(".user-sidemenu, .user-sidechart").addClass("sidebar-toggle__close");
          }

          if (!$sidemenu) {
              $(".archive-filter").attr("style", "margin-right: 0")
              sidebarToggle.setAttribute("class", "d-none");
          } else {
              new Switchery(sidebarToggle, {
                  color: "#D4D4D4",
                  secondaryColor: "#D4D4D4",
                  jackColor: '#029749',
                  jackSecondaryColor: '#5F6368'
              });
              $(".user-sidechart .user-sidechart__card").prop("style", "min-height: 1000px");
              setTimeout(() => {
                  $(".user-sidechart .user-sidechart__card").prop("style", "");
                  mainWidth();
              }, 300);
              $(sidebarToggle).on('change', function() {
                  mainWidth();
                  toggleSidebar();
              });
          }

          function toggleSidebar() {
              var sidemenu = $(".user-sidemenu, .user-sidechart");
              sidemenu.toggleClass("sidebar-toggle__close");
              if (window.innerWidth > 767) {
                  sessionStorage.sidebar = sidemenu.hasClass("sidebar-toggle__close") ? "off" : "on";
              } else {
                  sessionStorage.sidebar = "off";
              }
          }

          $(window).resize(function() {
              mainWidth();
          })

          function mainWidth() {
              var container = $(".container").width();
              if (window.innerWidth > 990) {
                  $(".user-sidechart").prop("style",
                      `max-width: ${container - (sidebarToggle.checked ? "270" : "90")}px`);
              } else {
                  $(".user-sidechart").prop("style", `max-width: ${window.innerWidth-30}px`);
              }
          }
          // Mendapatkan data dari local storage berdasarkan key
          function getDataFromLocalStorage(key) {
              const data = localStorage.getItem(key);
              // Periksa apakah data ditemukan dalam local storage
              if (data) {
                  return JSON.parse(data); // Mengembalikan data dalam bentuk objek JavaScript
              } else {
                  return null; // Jika data tidak ditemukan, kembalikan null
              }
          }

          const keyToRetrieve = "auth";
          const retrievedData = getDataFromLocalStorage(keyToRetrieve);
          const name = retrievedData.value.name
          const email = retrievedData.value.email

          $('.auth-name').html(name);
          $('.auth-email').text(email);

          $('.logout').click(function($q) {

              let title = "Logout Berhasil";
              let icon = "success";
              let iconColor = "#399E6B";
              let confirmButtonText = "<i class='fa fa-check-circle-o'></i> OK";
              let confirmButtonColor = "#399E6B";

              Swal.fire({
                  title,
                  icon,
                  iconColor,
                  confirmButtonText,
                  confirmButtonColor
              });


              setTimeout(function() {
                  //   window.location.reload();
              }, 3000);

              localStorage.removeItem("auth");
              window.location.href = "index.php"

          })

      })
  </script>
  </head>

  <body class="">
      <div id="wrapper" style="background-image: url(src/img/bg-homepage.png);">
          <!-- header -->
          <header id="header" class="header main-header">
              <div class="container">
                  <div class="navbar-header d-flex justify-content-between align-items-center">
                      <!-- toogle sidebar -->
                      <div id="sidebar-toggle-container">
                          <input type="checkbox" id="sidebar-toggle" data-color="#6EB842" checked />
                      </div>
                      <a style="position: absolute; left: 50%; -ms-transform: translateX(-50%); transform: translateX(-50%); display: -ms-flexbox; display: flex; -ms-flex-align: center; align-items: center;" href="<?= $base ?>index.php">
                          <img style="max-width: 70px;max-height: 36px;object-fit: contain;" src="<?= $base ?>assets/img/adago-white.png" alt="">
                      </a>
                      <div class="header-right d-flex align-items-center">
                          <span class="wa-help-center d-flex align-items-center p-relative">
                              <a href="https://wa.me/6282112484878" class="mr-16 whatsapp-message" target="_blank" style="width: 32px; line-height: 0;">
                                  <img src="<?= $base ?>assets/img/icon-access/ada_survey2.png" class="" alt="">
                              </a>
                              <div class="message">Halo, ada yang bisa kami bantu ?</div>
                          </span>
                          <div class="header-account">
                              <span class="header-user d-flex align-items-center">
                                  <img src="<?= $base ?>assets/img/user-icon.png" alt="">
                              </span>
                              <ul class="header-account__menu dark">
                                  <li class="menu-head">
                                      <img src="<?= $base ?>assets/img/user-icon.png" alt="">
                                      <div>
                                          <div class="auth-name"></div>
                                          <small class="auth-email"></small>
                                      </div>
                                  </li>
                                  <li class="menu-content">
                                      <ul>
                                          <li>
                                              <a class="logout"><i class="fa fa-sign-out"></i> Logout</a>
                                          </li>
                                      </ul>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>