<!DOCTYPE html>
<html lang="en">
<?php
$base = getenv('BASE_URL');
?>

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Adago Biometrik</title>


    <link rel="shortcut icon" href="<?= $base ?>assets/img/favicon.ico">
    <link rel="stylesheet" href="<?= $base ?>assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $base ?>assets/material-design-icon/materialdesignicons.min.css">
    <script>
        document.addEventListener("DOMContentLoaded", function() {

            /* change layout version 4 */
            /*
            	recommended ubah langsung jika sudah dipisah footer dan headernya di laravel (tidak menggunakan javascript).
            	seperti ini karena ada header custom di beberapa modul(html), jika di laravel sudah terpisah lebih baik langsung diubah di elementnya.
            */
            $("footer, .decoration-top").hide();
            var base_url = window.location.origin == "https://transisi.space" || window.location.origin == "http://transisi.space" ?
                window.location.origin + "/adago-html/" : window.location.origin + "/";

            // change background
            // $("#wrapper").css({
            //     "background-image": "url(" + base_url + "assets/img/bg-homepage.png",
            //     "background-repeat": "no-repeat",
            //     "background-position": "center",
            //     "background-size": "cover",
            //     "background-attachment": "fixed",
            // });

            // change header
            $(".header.main-header").css({
                "background": "linear-gradient(152.79deg, rgba(255,255,255, 0.3) 0%, rgba(255,255,255, 0) 100%)",
                "backdrop-filter": "blur(20px)",
            });

            var headerLogo = $(".header-logo img")[0];
            headerLogo?.setAttribute("src", base_url + "assets/img/adago-white.png");
            /* end recommended */

            /*
            	Add toggle sidebar if has sidebebar without toggle button in header
            	mainly on detail new window
            */
            var sidebarToggleContainer = document.getElementById("sidebar-toggle-container");
            var sidebarElement = document.querySelector(".user-sidemenu");
            !sidebarElement && $(".archive-filter").attr("style", "margin-right: 0");
            if (!sidebarToggleContainer && sidebarElement) {
                // create element to include switchery css
                var link = document.createElement("link");
                link.href = "<?= $base ?>assets/plugins/switchery/css/switchery.min.css";
                link.type = "text/css";
                link.rel = "stylesheet";

                // create element to include switchery js
                var switchery = document.createElement("script");
                switchery.type = "text/javascript";
                switchery.src = "<?= $base ?>assets/plugins/switchery/js/switchery.min.js";

                document.querySelector("head").appendChild(link);
                document.querySelector("head").appendChild(switchery);

                // create sidebar-toggle container
                var $sidebar = document.querySelector("#sidebar-toggle-container");
                var $header = document.querySelector(".navbar-header");
                var $toggle = `<div id="sidebar-toggle-container" style="position: absolute; left: 35px;">
				<input type="checkbox" id="sidebar-toggle" data-color="#6EB842" checked />
				</div>`;
                // insert sidebar-toogle in header
                $header.insertAdjacentHTML("beforeend", $toggle);

                // set switchery
                setTimeout(() => {
                    var $sidebar = document.querySelector("#sidebar-toggle-container");
                    var sidebarToggle = document.getElementById("sidebar-toggle");
                    var container = $(".container").width();

                    if (!$sidebar) {
                        sidebarToggle.setAttribute("class", "d-none");
                        // $(".archive-filter").attr("style", "margin-right: 0");
                    } else {
                        new Switchery(sidebarToggle, {
                            color: "#D4D4D4",
                            secondaryColor: "#D4D4D4",
                            jackColor: '#029749',
                            jackSecondaryColor: '#5F6368'
                        });
                        $(".user-sidechart .user-sidechart__card").prop("style", "min-height: 1000px");
                        $(".user-sidechart .user-sidechart__card").prop("style", "");
                        mainWidth();
                        $(sidebarToggle).on('change', function() {
                            mainWidth();
                            toggleSidebar();
                        });
                    }

                    $(window).resize(function() {
                        mainWidth();
                    })

                    function mainWidth() {
                        container = $(".container").width();
                        if (window.innerWidth > 990) {
                            $(".user-sidechart").prop("style", `max-width: ${container - (sidebarToggle.checked ? "270" : "90")}px`);
                        } else {
                            $(".user-sidechart").prop("style", `max-width: ${window.innerWidth-30}px`);
                        }
                    }
                }, 300);


                // set sessionStorage
                if (window.innerWidth > 767) !sessionStorage.sidebar && sessionStorage.setItem("sidebar", "on");
                else !sessionStorage.sidebar && sessionStorage.setItem("sidebar", "off");

                if (sessionStorage.sidebar == "on") {
                    $("#sidebar-toggle").prop("checked", true).trigger("change");
                    $(".user-sidemenu, .user-sidechart").removeClass("sidebar-toggle__close");
                } else {
                    $("#sidebar-toggle").prop("checked", false).trigger("change");
                    $(".user-sidemenu, .user-sidechart").addClass("sidebar-toggle__close");
                }

                // if (window.innerWidth > 990) $(".user-sidemenu").closest(".row").prop("flex-wrap", "no-wrap");
            }

            function toggleSidebar() {
                var sidemenu = $(".user-sidemenu, .user-sidechart");
                sidemenu.toggleClass("sidebar-toggle__close");
                // prevent toggle switchery after reload
                if (window.innerWidth > 767) {
                    sessionStorage.sidebar = sidemenu.hasClass("sidebar-toggle__close") ? "off" : "on";
                } else {
                    sessionStorage.sidebar = "off";
                }
            }


            $(".archive-filter").on("click", function() {
                $(this).toggleClass("open")
            })
        });
    </script>

    <style>
        .btn-outline--default {
            border: 1px solid #ababab !important;
            background: #eee !important;
            color: #444 !important;
        }

        .h-fit:is(.open) {
            height: fit-content !important;
        }

        @media (max-width: 991px) {
            .user-sidechart {
                max-width: 100% !important;
            }
        }
    </style>