<div class="row justify-content-between page-header">
    <div class="col-md-6 order-lg-2">
        <div class="archive-filter" style="border: none;">
            <div class="row">
                <div class="col text-center">
                    <a href="#" class= "active">
                        Biometric
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 order-lg-1">
        <div class="section-title">
            <h4>Biometric</h4>
            <img src="<?= $base ?>assets/img/short-variation.png" alt="">
        </div>
    </div>
</div>