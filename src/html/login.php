<!DOCTYPE html>
<html lang="en">
<?php
$base = getenv('BASE_URL');
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Adaro Web</title>
    <link rel="shortcut icon" href="assets/img/favicon.ico">

    <!-- Bootstrap 4.3.1 -->
    <link rel="stylesheet" href="<?= $base ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $base ?>assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $base ?>assets/plugins/select2/dist/css/select2.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?= $base ?>assets/css/adaro.css?v=<?= mt_rand() / mt_getrandmax(); ?>">
    <link rel="stylesheet" href="<?= $base ?>assets/css/login-new.min.css?v=<?= mt_rand() / mt_getrandmax(); ?>">
    <link rel="stylesheet" href="<?= $base ?>assets/css/font.min.css?v=<?= mt_rand() / mt_getrandmax(); ?>">
    <link rel="shortcut icon" href="<?= $base ?>assets/img/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        div.loading {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(16, 16, 16, 0.5);
            z-index: 9999;
        }

        @-webkit-keyframes uil-ring-anim {
            0% {
                -ms-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -webkit-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -ms-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -webkit-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-webkit-keyframes uil-ring-anim {
            0% {
                -ms-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -webkit-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -ms-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -webkit-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-moz-keyframes uil-ring-anim {
            0% {
                -ms-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -webkit-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -ms-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -webkit-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-ms-keyframes uil-ring-anim {
            0% {
                -ms-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -webkit-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -ms-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -webkit-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-moz-keyframes uil-ring-anim {
            0% {
                -ms-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -webkit-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -ms-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -webkit-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-webkit-keyframes uil-ring-anim {
            0% {
                -ms-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -webkit-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -ms-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -webkit-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-o-keyframes uil-ring-anim {
            0% {
                -ms-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -webkit-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -ms-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -webkit-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes uil-ring-anim {
            0% {
                -ms-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -webkit-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -ms-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -webkit-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .uil-ring-css {
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            width: 200px;
            height: 200px;
        }

        .uil-ring-css>div {
            position: absolute;
            display: block;
            width: 160px;
            height: 160px;
            top: 20px;
            left: 20px;
            border-radius: 80px;
            box-shadow: 0 6px 0 0 #ffffff;
            -ms-animation: uil-ring-anim 1s linear infinite;
            -moz-animation: uil-ring-anim 1s linear infinite;
            -webkit-animation: uil-ring-anim 1s linear infinite;
            -o-animation: uil-ring-anim 1s linear infinite;
            animation: uil-ring-anim 1s linear infinite;
        }
    </style>
    <style>
        .table-numb {
            color: #333333 !important;
        }

        #ajax-loading {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 9999 !important;
            width: 100%;
            height: 100%;
            display: none;
            background: rgba(0, 0, 0, 0.6);
        }

        .ajax-spinner {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .ajax-spinner .spinner {
            width: 40px;
            height: 40px;
            border: 4px #ddd solid;
            border-top: 4px #266141 solid;
            border-radius: 50%;
            animation: sp-anime 0.8s infinite linear;
        }

        @keyframes sp-anime {
            100% {
                transform: rotate(360deg);
            }
        }

        #ajax-loading {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 9999 !important;
            width: 100%;
            height: 100%;
            display: none;
            background: rgba(0, 0, 0, 0.6);
        }

        .ajax-spinner {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .ajax-spinner .spinner {
            width: 40px;
            height: 40px;
            border: 4px #ddd solid;
            border-top: 4px #266141 solid;
            border-radius: 50%;
            animation: sp-anime 0.8s infinite linear;
        }

        @keyframes sp-anime {
            100% {
                transform: rotate(360deg);
            }
        }
    </style>

</head>

<body>
    <div class="h-container" style="background:#f9fafc;">
        <div class="login-page d-flex justify-content-center align-items-center py-3">
            <div class="position-relative">

                <div class="login-container">
                    <div class="d-flex flex-wrap wrapper">
                        <div class="ornament" style="transform: translateY(38%);">
                            <img src="<?= $base ?>assets/img/biometric/biometric.svg" alt="">
                        </div>
                        <div class="login-form d-flex flex-column py-3" style="width: 417px;">
                            <form action="" method="POST">

                                <div class="section-title mb-3">
                                    <h4 class="mb-3" style="color: #066;">Masuk Akun</h4>
                                </div>

                                <div class="login-form-box mb-2">
                                    <input name="email" type="email" class="form-input font-weight-bold email" placeholder="Masukkan Email" value="admin@adaro.com">
                                    <span class="text-danger error-email"></span>
                                </div>

                                <div class="login-form-box form-date right mb-2">
                                    <div class="field">
                                        <input id="password" name="password" class="form-input pr-5 font-weight-bold password" name="password" type="password" value="123456" placeholder="Masukkan Kata Sandi / Password">
                                        <div class="icon-form d-flex align-items-center eye"><i class="fa fa-eye"></i></div>
                                    </div>

                                    <span class="text-danger error-password"></span>

                                </div>





                                <div class="login-form-box d-flex align-items-center justify-content-between remember my-24">
                                    <div>
                                        <input type="checkbox" class="checkbox-custom" name="remember" id="remember">
                                        <label for="remember" class="m-0"> <i class="fa fa-check"></i> Remember Me</label>
                                    </div>
                                </div>

                                <div class="form-box text center">
                                    <button type="submit" class="btn btn-submit font-weight-bold btn-login" name="login" style="border: none">Masuk Akun</button>
                                </div>
                            </form>

                        </div>
                    </div>
                    <div class="adaro-logo text-center my-16">
                        <img src="<?= $base ?>assets/img/biometric/logo-adaro.svg" style="width: 110px;" alt="">
                    </div>
                    <div class="copyright text-center">
                        Copyright © 2024 PT Adaro Indonesia.
                    </div>

                </div>

            </div>

        </div>
        <div id="ajax-loading">
            <div class="ajax-spinner">
                <span class="spinner"></span>
            </div>
        </div>
        <div class="loading d-none">
            <div class='uil-ring-css' style='transform:scale(0.79);'>
                <div></div>
            </div>
        </div>
        <!-- jQuery 3 -->
        <script src="<?= $base ?>assets/js/jquery.min.js"></script>
        <script src="<?= $base ?>src/js/env.js"></script>
        <script src="<?= $base ?>assets/js/jquery-validate.min.js"></script>
        <script src="<?= $base ?>assets/plugins/select2/dist/js/select2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

        <script>
            $(document).ready(function() {

                const isLogin = localStorage.getItem('auth');
                console.log(isLogin);
                $(".select2").select2();

                $('.eye').on('click', function(e) {
                    e.preventDefault
                    var $icon = $(this).find('i');
                    if ($icon.hasClass('fa-eye')) {
                        $icon.prop("class", 'fa fa-eye-slash');
                        $(this).parent().find('input:password').prop('type', 'text');
                    } else {
                        $icon.prop("class", 'fa fa-eye');
                        $(this).parent().find('input:text').prop('type', 'password');
                    }

                });

                var $waFloat = document.querySelector(".whatsapp-float");
                if (window.innerWidth < 601) $waFloat.style.right = "-10px";
                $(window).on("resize", () => $waFloat.style.right = window.innerWidth < 601 ? "-10px" : "30px")



            });
        </script>
        <script>
            $(document).ready(function() {

                $(".btn-login").click(function(e) {
                    e.preventDefault();

                    var url = base_url
                    var email = $(".email").val();
                    var password = $(".password").val();
                    var token = $("meta[name='csrf-token']").attr("content");

                    if (email.length == "") {
                        $('.error-email').html('username wajib diisi')

                    } else if (password.length == "") {
                        $('.error-password').html('password wajib diisi')

                    } else {
                        $(`.loading`).removeClass('d-none');
                        $.ajax({
                            url: url + "login",
                            type: "post",
                            dataType: "JSON",
                            data: {
                                "email": email,
                                "password": password,
                            },

                            success: function(response) {

                                if (response.success) {
                                    console.log(response);

                                    const keyToRemove = "auth";
                                    removeItemFromLocalStorage(keyToRemove);

                                    const expirationHours = 2; // Waktu kedaluwarsa dalam jam
                                    setLocalStorageWithTimer("auth", response.data, expirationHours);


                                    toastr.success('Berhasil Login');
                                     window.location.href = "home.php"


                                } else {
                                    console.log(response);
                                }

                            },

                            error: function(response) {
                                $(`.loading`).addClass('d-none');
                                let message = response.responseJSON.message;

                                toastr.error(message);

                            }

                        });

                    }

                });

            });




            // Fungsi untuk menyimpan data ke local storage dengan key dan value
            function setLocalStorageWithTimer(key, value, expiration) {
                const now = new Date();
                const item = {
                    value: value,
                    expiration: now.getTime() + expiration * 60 * 60 * 1000 // Menambahkan waktu kedaluwarsa (dalam milidetik)
                };
                localStorage.setItem(key, JSON.stringify(item));
            }

            // Fungsi untuk mengambil data dari local storage dengan key
            function getLocalStorage(key) {
                const itemStr = localStorage.getItem(key);
                // Jika item tidak ditemukan atau sudah kedaluwarsa, kembalikan null
                if (!itemStr) {
                    return null;
                }
                const item = JSON.parse(itemStr);
                const now = new Date();
                // Periksa apakah item telah kedaluwarsa
                if (now.getTime() > item.expiration) {
                    // Jika telah kedaluwarsa, hapus item dari local storage
                    localStorage.removeItem(key);
                    return null;
                }
                return item.value;
            }


            // Menghapus item dari local storage dengan key tertentu
            function removeItemFromLocalStorage(key) {
                localStorage.removeItem(key);
            }
        </script>

</body>

</html>