// Sandstrap POPUP
// version 1.0.0
// © Fursandy Anggita, 2019
// fursandyanggita@gmail.com
// =============================================

(function($) {
    // Event Listener
    $(document).on("click", '[data-toggle="st-popup"]', openPopup);
    // $('[data-toggle="st-popup"]').on("click", openPopup);
    $(".popup-close").on("click", closePopup);

    $("body").on("click", ".backdrop-popup, .st-popup-wrap", function(e) {
        if ($(this).data("backdrop") == "static") {
            e.stopPropagation();
        } else {
            if ($(e.target).parents(".popup-box").length == 0) {
                closePopup();
                e.stopPropagation();
            }
        }
    });

    $(".st-popup-wrap .popup-box").on("click", function(e) {
        // e.stopPropagation();
    });

    $(".st-popup-wrap").on("show.st-popup", function() {
        var popupHeight = $(this)
            .find(".popup-box")
            .outerHeight(true);
        var winHeight = $(window).height();

        if (popupHeight > winHeight) {
            $(this).addClass("popup-overflow-scroll");
        }
    });

    // Init
    $(".st-popup-wrap").each(function() {
        $(this).hide();
        $(this).appendTo($("body"));
    });

    // Methods
    function closePopup() {
        $(".st-popup-wrap").removeClass("show");

        window.sourceModal = null;

        setTimeout(function() {
            $("body").removeClass("st-popup-open");
        }, 200);
        setTimeout(function() {
            $(".backdrop-popup").remove();
            $(".st-popup-wrap").hide();
            $(document).trigger("hide.st-popup");
        }, 1000);
    }

    function openPopup(e) {
        var source = e.target.getAttribute("data-source");
        window.sourceModal = source && JSON.parse(source);
        window.sourceTarget = e.target;

        var target = this.targetPopup || $(this).data("target");
        var backdrop = '<div class="backdrop-popup"></div>';

        if ($(".backdrop-popup").length == 0) {
            $(backdrop).appendTo($("body"));
        }

        $(target).show();

        setTimeout(function() {
            $("body").addClass("st-popup-open");
            $(target).addClass("show");

            $(target).trigger("show.st-popup");
        }, 30);

        e && e.preventDefault();
    }

    // Expose
    window.sandstrapPopup = {
        open: openPopup,
        close: closePopup
    };
})(jQuery);
